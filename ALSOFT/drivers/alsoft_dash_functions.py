'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

import dash_bootstrap_components as dbc
from dash import  dcc, html, dash_table
import pandas as pd
import plotly.graph_objs as go


def create_all_variable_box(description, value,explained, id_suffix):
    """
    Creates an HTML div containing an input box with its description and explanation.

    Parameters:
        description (str): Description text for the variable.
        value (float): Initial value of the input box.
        explained (str): Explanation text for the variable.
        id_suffix (int): Suffix for the unique ID of the input box.

    Returns:
        html.Div: A div containing the description, input box, and explanation.
    """
    
    return html.Div([
        html.Div(description, style={'width': '20%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px', 'text-align': 'center'}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'all-variable-box','index': id_suffix}, style={"width": "28%", "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),
        html.Div(explained, style={'width': '50%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '12px', 'marginLeft': '5px', 'padding-top': '20px'})
    ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'})

def create_input_box(description, value, id_suffix):
    """
    Creates an HTML div containing an input box with its description.

    Parameters:
        description (str): Description text for the input.
        value (float): Initial value of the input box.
        id_suffix (int): Suffix for the unique ID of the input box.

    Returns:
        html.Div: A div containing the description and input box.
    """
    
    return html.Div([
        html.Div(description, style={'width': '60%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px'}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'input-box','index': id_suffix}, style={"width": "40%", "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px'})
    ], style={"width": "100%", "margin-bottom": "5px", "margin-top": "5px"})

def create_datatable(input_list, input_desc, id_suffix):
    """
    Creates a Dash DataTable based on input data and descriptions.

    Parameters:
        input_list (list): List of input values.
        input_desc (list): List of descriptions for the input values.
        id_suffix (int): Suffix for the unique ID of the datatable.

    Returns:
        html.Div: A div containing the Dash DataTable.
    """

    data_dict = {
        input_desc[i]: input_list[i]
        for i in range(len(input_desc))}
    df = pd.DataFrame(data_dict)

    return html.Div(
            dash_table.DataTable(
            data=df.to_dict('records'),
            columns=[{'id': c, 'name': c} for c in df.columns],
            
            id={'type': 'datatable','index': id_suffix},
            style_data={
                'color': 'black',
                'backgroundColor': 'white',
                'textAlign': 'center',
                'font-family': 'Computer Modern'
            },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(220, 220, 220)',
                }
            ],
            style_header={
                'backgroundColor': 'rgb(210, 210, 210)',
                'color': 'black',
                'fontWeight': 'bold',
                'textAlign': 'center',
                'font-family': 'Computer Modern'
            },
            editable=True
        )
    )




def create_plot_data(result_data, data):
    """
    Creates a dictionary of plots based on result data.

    Parameters:
        result_data (dict): Dictionary containing the result data.
        data (dict): Dictionary containing the current state of data.

    Returns:
        dict: Updated dictionary containing plot data.
    """
    
    for key in list(result_data.keys()):
        result_data[key] = result_data[key][1:]
    
    if data['plot'] is None: 
        plot_a = go.Scatter(x=result_data['time'], y=result_data['x'], name = "Plot 1")
        plot_b = go.Scatter(x=result_data['time'], y=result_data['d'], name = "Plot 1")
        plot_c = go.Scatter(x=result_data['time'], y=result_data['rp02'], name = "Plot 1")
        plot_d = go.Scatter(x=result_data['time'], y=result_data['rho_i'], name = "Plot 1")
        plot_e = go.Scatter(x=result_data['time'], y=result_data['delta'], name = "Plot 1")
        plot_f = go.Scatter(x=result_data['time'], y=result_data['r'], name = "Plot 1")

        new_dict = {
            "plot-a": plot_a,
            "plot-b": plot_b,
            "plot-c": plot_c,
            "plot-d": plot_d,
            "plot-e": plot_e,
            "plot-f": plot_f
            
            
        }
        
    else:
        old_dict = data['plot']
        index = len(old_dict.get("plot-a1", [])) + 2
        new_trace_a = go.Scatter(x=result_data['time'], y=result_data['x'], name = f"Plot {index}")
        new_trace_b = go.Scatter(x=result_data['time'], y=result_data['d'], name = f"Plot {index}")
        new_trace_c = go.Scatter(x=result_data['time'], y=result_data['rp02'], name = f"Plot {index}")
        new_trace_d = go.Scatter(x=result_data['time'], y=result_data['rho_i'], name = f"Plot {index}")
        new_trace_e = go.Scatter(x=result_data['time'], y=result_data['delta'], name = f"Plot {index}")
        new_trace_f = go.Scatter(x=result_data['time'], y=result_data['r'], name = f"Plot {index}")

        
        if "plot-a1" not in old_dict:
            old_dict["plot-a1"] = [new_trace_a]
            old_dict["plot-b1"] = [new_trace_b]
            old_dict["plot-c1"] = [new_trace_c]
            old_dict["plot-d1"] = [new_trace_d]
            old_dict["plot-e1"] = [new_trace_e]
            old_dict["plot-f1"] = [new_trace_f]

        else:
            old_dict["plot-a1"].append(new_trace_a)
            old_dict["plot-b1"].append(new_trace_b)
            old_dict["plot-c1"].append(new_trace_c)
            old_dict["plot-d1"].append(new_trace_d)
            old_dict["plot-e1"].append(new_trace_e)
            old_dict["plot-f1"].append(new_trace_f)
            
        new_dict = old_dict
    
    data['plot'] = new_dict
    
    return data['plot']




def create_plots_from_plot_data(plot_data):
    """
    Creates Plotly graphs from the provided plot data.

    Parameters:
        plot_data (dict): Dictionary containing plot data.

    Returns:
        html.Div: A div containing multiple plotly graphs.
    """
    
    plot_a = go.Figure(data=plot_data["plot-a"])
    plot_a.update_layout(
        title={'text': 'Fraction recrystallized' , 'x': 0.5},
        xaxis_title='time   [s]',
        yaxis_title='fraction recrystallized   X',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.0%', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10, 
            r=10, 
        )
    )

    plot_b = go.Figure(data=plot_data["plot-b"])
    plot_b.update_layout(
        title={'text': 'Mean rx grain size', 'x': 0.5},
        xaxis_title= 'time   [s]',
        yaxis_title='subgrain radius r [m]',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.1e', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10, 
            r=10, 
        )
    )

    plot_c = go.Figure(data=plot_data["plot-c"])
    plot_c.update_layout(
        title={'text': 'Yield strength' , 'x': 0.5},
        xaxis_title= 'time   [s]',
        yaxis_title='offset yield Rp02  [MPa] ',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.0f', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30,
            b=10, 
            l=10, 
            r=10, 
        )
    )
    
    plot_d = go.Figure(data=plot_data["plot-d"])
    plot_d.update_layout(
        title={'text':'Dislocation density' , 'x': 0.5},
        xaxis_title= 'time   [s]',
        yaxis_title='dislocation density \u03c1\u1d62 [m\u207b\u00b2]',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.1e', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10,
            r=10, 
        )
    )
    
    max_y_value_plot_e = max(plot_data["plot-e"]["y"]) 
    
    plot_e = go.Figure(data=plot_data["plot-e"])
    plot_e.update_layout(
        title={'text': 'Average subgrain size', 'x': 0.5},
        xaxis_title= 'time   [s]',
        yaxis_title='subgrain diameter   \u03b4 [m]',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.1e', automargin=True, range=[0, max_y_value_plot_e]),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10,
            l=10, 
            r=10, 
        )
    )
    
    plot_f = go.Figure(data=plot_data["plot-f"])
    plot_f.update_layout(
        title={'text': 'Radius of recrystallized grains', 'x': 0.5},
        xaxis_title= 'time   [s]',
        yaxis_title='subgrain radius [m]',
        xaxis=dict(tickformat='.1e', automargin = True),
        yaxis=dict(tickformat='.1e', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10,
            l=10, 
            r=10, 
        )
    )
    
    if "plot-a1" in plot_data:
        for value_a in plot_data["plot-a1"]:
            plot_a.add_trace(value_a)
        for value_b in plot_data["plot-b1"]:
            plot_b.add_trace(value_b)
        for value_c in plot_data["plot-c1"]:
            plot_c.add_trace(value_c)
        for value_d in plot_data["plot-d1"]:
            plot_d.add_trace(value_d)
        for value_e in plot_data["plot-e1"]:
            plot_e.add_trace(value_e)
        for value_f in plot_data["plot-f1"]:
            plot_f.add_trace(value_f)
            
            
        plot_a.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.05, 'y': 0.95})
        plot_b.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.05, 'y': 0.95})
        plot_c.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.05, 'y': 0.05})
        plot_d.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.95})
        plot_e.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.05, 'y': 0.95})
        plot_f.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.05, 'y': 0.95})

        
    content = html.Div([
        dbc.Row([
            dbc.Col(dcc.Graph(figure=plot_a, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_b, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_c, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_d, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_e, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_f, style={'height': '300px'}), width=4)
            ])
    ],
        style={'margin-left': '0%', 'padding-top': '5px'}
    )
    
    plot_a.update_xaxes(type='log')
    plot_b.update_xaxes(type='log')
    plot_c.update_xaxes(type='log')
    plot_d.update_xaxes(type='log')
    plot_e.update_xaxes(type='log')
    plot_f.update_xaxes(type='log')
    return content


def update_variables_table(input_values, datatables, data):
    """
    Updates the variables table based on input values and datatables.

    Parameters:
        input_values (list): List of input values.
        datatables (list): List of datatable values.
        data (dict): Dictionary containing the current state of data.

    Returns:
        dict: Updated dictionary containing the updated data.
    """
    
    updated_standard_input = data['standard_input'].copy()  
    for index, value in enumerate(input_values):
        updated_standard_input[index] = float(value)
    
    for index, values in enumerate(datatables):
        table_dics = datatables[index]
        keys = table_dics[0].keys()        
        result = {key: [] for key in keys}

        for dic in table_dics:
            for key in dic:
                result[key].append(float(dic[key])) 
        updated_input_data = list(result.values())
 
    updated_data = data
    updated_data['standard_input'] = updated_standard_input
    updated_data['datatable'] = create_datatable(updated_input_data , data['input_desc'],0)
    updated_data['input_data'] = updated_input_data
    
    return updated_data

def update_variables_input(all_variable_values, data):
    """
    Updates the variables input based on all variable values.

    Parameters:
        all_variable_values (list): List of all variable values.
        data (dict): Dictionary containing the current state of data.

    Returns:
        dict: Updated dictionary containing the updated variables.
    """
    
    updated_variables_data = data['fm'].copy()  
    i = -9 # This is very dependent on the Dash layout (it reads top-bottom)
    
    for value in all_variable_values:
        updated_variables_data[i] = float(value) # Retrieve index from input ID
        i+=1
    updated_data = data
    updated_data['fm'] = updated_variables_data
    
    return updated_data

