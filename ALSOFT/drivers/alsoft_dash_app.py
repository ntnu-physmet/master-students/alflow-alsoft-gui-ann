'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

import dash
import dash_bootstrap_components as dbc
from dash import Input, Output, dcc, html, State
from dash.dependencies import ALL
import pandas as pd
import os
from alsoft_functions import running_alsoft, initialize_input, use_datafile, initialize_table_values, calculate_z_rho_delta, sci_form
from alsoft_dash_functions import create_all_variable_box,  create_datatable, create_plot_data, create_plots_from_plot_data, update_variables_table, update_variables_input, create_input_box

input_datafile = ("./data/1xxx.inp")
fm, fm_desc, fm_explained = initialize_input(input_datafile)

standard_input = [1000, 520, 0.0053138, 4500, 10]
input_data, input_desc = initialize_table_values(standard_input)

editable_table = create_datatable(input_data,input_desc,0)
all_variable_boxes = [create_all_variable_box(description, value, explained , i) 
                      for i, (description, value,explained) in enumerate(zip(fm_desc, fm , fm_explained))]

data_dict = {
    'input_data': input_data,
    'input_desc': input_desc,
    'standard_input': standard_input,
    'fm': fm,
    'fm_desc': fm_desc,
    'fm_explained': fm_explained,
    'datatable': editable_table,
    'plot' : None,
    'alsoft_results' : None
    }
          
app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = dbc.Container(
    [
        dcc.Store(id="store_data", data = data_dict),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Nav(
                        [
                            dbc.NavLink("Run ALSOFT", href="#", id="run-alsoft-button", style={'color': 'blue'}),
                            html.Div(style={'height': '20px'}),  # Empty div for spacing
                            dbc.NavLink("Download results", href="#", id="btn-download-txt", style={'color': 'black'}),
                            dbc.NavLink("Reset results", href="#", id="reset-alsoft-button", style={'color': 'red'}),
                            dcc.Download(id="download-text"),
                        ],
                        vertical=True,
                        pills=True,
                    ),
                    width=2,
                    className="bg-light",
                    style={'position': 'fixed', 'top': 0, 'left': 0, 'height': '100vh', 'padding': '40px', 'width':'30vh', 'font-size': '18px'},
                ),
                dbc.Col(
                    dbc.Tabs(
                        [                              
                            dbc.Tab(label="Plots", tab_id="plot"),
                            dbc.Tab([
                                dbc.Row([ 
                                    dbc.Col([
                                        dbc.Button(
                                            "Update parameters", id="update-variable-button-table", outline=True, color="primary", className="me-1"
                                        ),
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Apply values to table", id="reset-parameters-button", outline=True, color="secondary", className="me-1"
                                            ),
                                        ], width=3),
                                ], style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}),
                                ] , label="Table", tab_id="table"),
                            dbc.Tab([
                                dbc.Row([ 
                                    dbc.Col([
                                        dbc.Button(
                                            "Update parameters", id="update-variable-button-input", outline=True, color="primary", className="me-1"
                                        ),
                                    ], width=3),
                                    
                                    dbc.Col([
                                        dbc.Button(
                                            "Calculate Z, rho, delta", id="calculate-button", outline=True, color="success", className="me-1"
                                        ),
                                    ], width=3),
                                    dbc.Col([
                                        dcc.Dropdown(
                                            id='file-dropdown',
                                            options=[
                                                {'label': file, 'value': file} for file in os.listdir('./data')],
                                            placeholder='Select a file...')
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Use selected file", id="use-datafile-button", outline=True, color="secondary", className="me-1"
                                        ),
                                    ], width=3)
                                    
                                ],
                                    style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}
                                ),
                            ], label="Input", tab_id="input"),
                        ],
                        id="tabs",
                        active_tab="plot",
                    ),
                    width=10,
                    style={'margin-left': '10%', 'padding-top': '15px'},
                ),
            ]
        ),
        html.Div(id="tab-content", className="p-4", style={'margin-left': '10%', 'padding-top': '20px'}),
    ]
)


@app.callback(
    Output("tab-content", "children"),
    [Input("tabs", "active_tab"), Input("store_data", "data")],
)
def render_tab_content(active_tab,  data):
    """
    Renders the content of the selected tab.
    
    Parameters:
        active_tab (str): The currently active tab.
        data (dict): The stored data.

    Returns:
        content (html.Div): The content to display based on the active tab.
    """
    
    plot_data = data['plot']
    if active_tab == "plot" and plot_data is not None:
        content = create_plots_from_plot_data(plot_data)
        return content
        
    elif active_tab == "table":
        input_var = data['input_data']
        input_desc = data['input_desc']
        standard_input = data['standard_input']
        
        input_boxes = [create_input_box(description, value, i) 
                       for i, (description, value) in enumerate(zip(input_desc, standard_input))]
        input_boxes.append(create_input_box("Lines", 10, 4))
        editable_table = create_datatable(input_var, input_desc, 0)
        
        content = html.Div([
            dbc.Row([
                dbc.Col([
                    dbc.Row(html.Div(input_boxes))
                    ], width = 3),
                dbc.Col(html.Div(editable_table), width = 7),
            ])
        ])
        return content
    
    elif active_tab == "input":
        all_variables = data['fm']
        all_variables_descriptions = data['fm_desc']
        all_variables_explained = data['fm_explained']
        all_variable_boxes = [create_all_variable_box(description, value, explained , i) 
                              for i, (description, value,explained) in enumerate(zip(
                                      all_variables_descriptions, all_variables , all_variables_explained))]
        
        content = html.Div([
            dbc.Row([
                dbc.Col(html.Div(all_variable_boxes[45:49]), width = 5),
                dbc.Col(html.Div(all_variable_boxes[49:]), width = 5, style={'margin-left': '10%'})
            ]),
            dbc.Row([
                html.Hr(style={'borderWidth': "0.3vh", "width": "100%", "color": "#808080"})                
            ]),
            
            dbc.Row([
                dbc.Col(html.Div(all_variable_boxes[0:22]), width = 5),
                dbc.Col(html.Div(all_variable_boxes[22:45]), width = 5, style={'margin-left': '10%'})
            ])
        ])
        return content

    return "Press Run ALSOFT for results"

@app.callback(
    Output("download-text", "data"),
    [Input("btn-download-txt", "n_clicks"), State("store_data", "data")],
    prevent_initial_call=True
)
def download_file(n_clicks, data):
    """
    Downloads the results as an Excel file when the download button is clicked.
    
    Parameters:
        n_clicks (int): Number of times the download button was clicked.
        data (dict): The stored data.

    Returns:
        dcc.send_file: A Dash component that initiates the file download.
    """
    
    if n_clicks and data['alsoft_results'] is not None:
        writer = pd.ExcelWriter("AlsoftData.xlsx", engine='xlsxwriter')
        
        for i in range(len(data['alsoft_results'])):
            df = pd.DataFrame(data['alsoft_results'][i])  
            sheet_name = f'Plot{i+1}'
            df.to_excel(writer, sheet_name=sheet_name)
        writer.close()
        return dcc.send_file("AlsoftData.xlsx")

    return dash.no_update


@app.callback(
    Output("store_data", "data"),
    [State({"type": "all-variable-box", "index": ALL}, "value"),
    State({"type": "input-box", "index": ALL}, "value"),
    State({"type": "datatable", "index": ALL}, "data"),
    Input("run-alsoft-button", "n_clicks"),
    Input("update-variable-button-table", "n_clicks"),
    Input("update-variable-button-input", "n_clicks"),
    Input("use-datafile-button", "n_clicks"),
    Input("calculate-button", "n_clicks"),
    Input("reset-alsoft-button", "n_clicks"),
    Input("reset-parameters-button", "n_clicks"),
    State("store_data", "data"),
    State('file-dropdown', 'value')
    ],
)


def update_input_values(all_variable_values, input_values, datatables, n_clicks_rund_alflow , n_clicks_var_tab, n_clicks_var_inp,
                        n_clicks_dropdown, n_clicks_calculate, n_clicks_reset , n_clicks_reset_table, data, selected_file):
    """
    Updates the stored data based on user interactions.

    Parameters:
        all_variable_values (list): List of values from all variable input boxes.
        input_values (list): List of values from input boxes.
        datatables (list): List of data from data tables.
        n_clicks_rund_alflow (int): Number of times the "Run ALSOFT" button was clicked.
        n_clicks_var_tab (int): Number of times the "Update parameters" button (Table tab) was clicked.
        n_clicks_var_inp (int): Number of times the "Update parameters" button (Input tab) was clicked.
        n_clicks_dropdown (int): Number of times the "Use selected file" button was clicked.
        n_clicks_calculate (int): Number of times the "Calculate Z, rho, delta" button was clicked.
        n_clicks_reset (int): Number of times the "Reset results" button was clicked.
        n_clicks_reset_table (int): Number of times the "Apply values to table" button was clicked.
        data (dict): The stored data.
        selected_file (str): The file selected from the dropdown menu.

    Returns:
        dict: Updated stored data.
    """
    
    ctx = dash.callback_context
    if not ctx.triggered:
        return dash.no_update
    triggered_id = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if triggered_id == 'run-alsoft-button':        
        input_list_alsoft = data['input_data']
        fm = data['fm'][0:49]
        L = running_alsoft(fm,input_list_alsoft)
        L_df = pd.DataFrame(L)
        N_df = L_df.T

        result_data = {
            'time': N_df[0],
            'rho_i': N_df[1],
            'delta': N_df[2],
            'r': N_df[3],
            'temp': N_df[4],
            'css': N_df[5],
            'pz': N_df[6],
            'pd': N_df[7],
            'mobility': N_df[8],
            'growth': N_df[9],
            'g': N_df[10],
            'rp02': N_df[11],
            'x': N_df[12],
            'xext': N_df[13],
            'd': N_df[14],
            'z': N_df[15],
            'strain_def': N_df[16],
            'ntot': N_df[17],
            'ncube': N_df[18],
            'npsn': N_df[19],
            'fcube': N_df[20],
            'fgb': N_df[21],
            'fpsn': N_df[22]
            
        }
        if data['alsoft_results'] is None:
            data['alsoft_results'] = [result_data]
        else:             
            result_list = data['alsoft_results']
            result_list.append(result_data)
            data['alsoft_results'] = result_list
        
        plot_data = create_plot_data(result_data, data)
        data['plot'] = plot_data
        return data
    
    elif triggered_id == 'reset-alsoft-button':        
        data['plot'] = None
        data['alsoft_results'] = None
        return data
    
    
    
    elif triggered_id == 'update-variable-button-table':
        updated_data = update_variables_table(input_values, datatables, data)
        return updated_data
    
    elif triggered_id == 'update-variable-button-input':
        updated_data = update_variables_input(all_variable_values, data)
        return updated_data
    
    
    
    elif triggered_id == "use-datafile-button":
        if selected_file != None:
            use_datafile(selected_file, input_datafile)
            fm, fm_desc, fm_explained = initialize_input(input_datafile)
            data['fm'] = fm
            data['fm_desc'] = fm_desc
            data['fm_explained'] = fm_explained
            
            return data
        
    elif triggered_id == 'calculate-button':
        updated_data = update_variables_input(all_variable_values, data)

        fm = updated_data['fm']
        Z, rho_i , delta = calculate_z_rho_delta(fm)
        
        updated_data['fm'][48] = sci_form(Z)
        updated_data['fm'][1] = sci_form(rho_i)
        updated_data['fm'][2] = sci_form(delta)
        return updated_data
    
    elif triggered_id == 'reset-parameters-button':
        standard_values = data['standard_input'].copy()
        for index, value in enumerate(input_values):
            standard_values[index] = float(value) 
        
        data['standard_input'] = standard_values
        input_data, input_desc = initialize_table_values(standard_values)
        data['input_data'] = input_data
        editable_table = create_datatable(input_data,input_desc,0)
        data['datatable'] = editable_table
        return data
    
    return dash.no_update

if __name__ == "__main__":
    app.title = "ALSOFT"
    app.run_server(debug = True, port=2526) #for local testing
    #port = os.environ.get('dash_port')
    #debug = os.environ.get('dash_debug')=="True"
    #app.run_server(debug = debug, host="0.0.0.0", port=port)
