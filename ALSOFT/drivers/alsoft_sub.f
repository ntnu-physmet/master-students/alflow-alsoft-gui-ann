      subroutine alsoft_sub(input_data, treatment_params, output_data)
      implicit none
C-----------------------------------------------------------------------
C     Daniel Preminger 25-1-2024
C
C     Subroutine version of alsoft
C-----------------------------------------------------------------------

	  double precision , dimension(49) :: input_data
Cf2py intent(in) :: input_data     
	  double precision , dimension(4,23) :: treatment_params
Cf2py intent(in) :: treatment_params
      double precision, dimension(23,3000) :: output_data
Cf2py intent(out) :: output_data 

	  
	  double precision, dimension(23) :: time_array
	  double precision, dimension(23) :: T_array
	  double precision, dimension(23) :: C_array
	  double precision, dimension(23) :: PZ_array
	  
	  integer trt_len
	  integer trt_I, out_L
	  
	  double precision rho_i, delta, r,
     $     time1, time2, T1, T2, Css1, Css2, PZ1, PZ2,
     $     timestep,
     $     X, D, Rp02
	  
	  trt_len = 23
	  out_L = 0
	  
	  time_array = treatment_params(1, 1:23)
	  T_array = treatment_params(2, 1:23)
	  C_array = treatment_params(3, 1:23)
	  PZ_array = treatment_params(4, 1:23)




C-----------------------------------------------------------------------




C  -- Read parameters
      call as_init(input_data, rho_i, delta, r)


C  -- Read first row in treatment table	
  
	  time2 = time_array(1)
	  T2 = T_array(1)
	  Css2 = C_array(1)
	  PZ2 = PZ_array(1)
	  T2   = T2 + 273.15


C  -- Calculate initial state, by calling the integrator with time1 
C     equal to time2
      timestep = 0.0
      call as_int(rho_i, delta, r, output_data, out_L, 
     $     time2, time2, T2, T2, Css2, Css2, PZ2, PZ2, 
     $     timestep)



      call as_calc(X, D, Rp02)



C  -- Integrate over the rest of heat treatment table
	  Do trt_I = 2, trt_len
         time1 = time2
         T1    = T2
         Css1  = Css2
         PZ1   = PZ2
		 
   	     time2 = time_array(trt_I)
	     T2 = T_array(trt_I)
	     Css2 = C_array(trt_I)
	     PZ2 = PZ_array(trt_I)
	     T2   = T2 + 273.15
         
         timestep = 0.01*time2

         call as_int(rho_i, delta, r, output_data, out_L,
     $        time1, time2, T1, T2, Css1, Css2, PZ1, PZ2,
     $        timestep)

      call as_calc(X, D, Rp02)


      end do




      end
