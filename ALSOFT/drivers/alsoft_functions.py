'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

import alsoft_sub
import shutil
import numpy as np                        
import math
import copy

def running_alsoft(fm ,input_data):
    """
    Use the Fortran Extension Module for ALFLOW computations.

    Parameters:
    fm (list): List of the 49 parameters in the format [p0, p1, p2, ..., p48].
    input_data (list): A 2D list containing softening conditions temperature, effective concentration and PZ for every time in the table.

    Returns:
    list: A list containing the output data from the Fortran module.
    """
    input_table = copy.deepcopy(input_data)
    input_len = len(input_table[0])
    for i in range(23):
        if i>=input_len:
            for j in range(0,4):
                input_table[j].append(input_table[j][i-1])

    output = alsoft_sub.alsoft_sub(fm, input_table)
    for i in range(1, len(output[0])):
        if output[0][i] == 0:
            output = [sublist[:i] for sublist in output]
            break
    output[11] = output[11]*1e-6 # return in MPa instead of Pa
    return output


Rgas = 8.314

def fZ(strainRate, U_ZH, T_def):
    """
    Calculate the Zener-Holomon parameter.

    Parameters:
    strainRate (float): The strain rate [1/s].
    U_ZH (float): The activation energy [J/mol].
    T_def (float): The deformation temperature [K].

    Returns:
    float: The Zener-Holomon parameter.
    """
    
    Z = strainRate*math.exp(U_ZH/(Rgas*T_def))
    return Z

def finitZH(Z, T_def, As, Bs, C1):
    """
    Use Newton's method to find the delta value and calculate rho_i.

    Parameters:
    Z (float): The Zener-Holomon parameter.
    T_def (float): The deformation temperature [K].
    As (float): A constant specific to the material.
    Bs (float): A constant specific to the material.
    C1 (float): A constant specific to the material.

    Returns:
    tuple: A tuple containing rho_i and delta.
    """
    
    delta = 1e-9  # starting value for iteration
    epsilon = 1e-4
    rel_diff = 1

    while abs(rel_diff) > epsilon:  # Newton's method
        f = 1 / delta - (Rgas*T_def/As)*math.log(Z*delta**2/Bs)
        df = -1/(delta**2) - 2*Bs*Rgas*T_def/(As*Z*delta)
        delta_new = delta - f / df
        rel_diff = (delta_new - delta) / delta
        delta = delta_new

    rho_i = (C1 / delta)**2
    return rho_i, delta

def calculate_z_rho_delta(fm):
    """
    Calculate the Zener-Holomon parameter, rho_i, and delta using the parameters from the input data.

    Parameters:
    fm (list): The list of parameters including strainRate, U_ZH, As, Bs, and C1.

    Returns:
    tuple: A tuple containing the Zener-Holomon parameter, rho_i, and delta.
    """
    
    strainRate, U_ZH, As, Bs, C1 = fm[-5], fm[-4], fm[-3], fm[-2], fm[-1]
    
    T_def = fm[45] + 273.15
    
    Z = fZ(strainRate, U_ZH, T_def)
    rho_i, delta =  finitZH(Z, T_def, As, Bs, C1)
    
    return Z , rho_i , delta


def sci_form(x):
    """
    Convert a number to scientific notation if it's very large or very small. Used in application.

    Parameters:
    x (float): The number to format.

    Returns:
    str: The formatted number as a string.
    """
    
    if (abs(x) < 0.001 or abs(x) > 1000) and x != 0:
        x="{:.3e}".format(x)
    else:
        x="{:.3f}".format(x)
    return x


def initialize_input(input_datafile):
    """
    Read the alloy data file and initialize the parameter list.

    Parameters:
    input_datafile (str): The path to the input data file.

    Returns:
    tuple: A tuple containing the parameter list, parameter descriptions, and explanations.
    """
    
    i_df = open(input_datafile, "r")
    input_filedata = i_df.read() 
    input_filelines = input_filedata.splitlines()
    i_df.close()
    
    list_elements = 49  
    fm = [0]*list_elements
    fm_desc = [0]*list_elements
    fm_explained = [0]*list_elements
    
    for i in range(list_elements):
        float_string = input_filelines[i][0:11].strip()
        fm[i] = float(float_string)
        if (abs(fm[i]) < 0.001 or abs(fm[i]) > 1000) and fm[i] != 0:
            fm[i]=sci_form(fm[i])
        fm_desc_string = input_filelines[i][15:27].strip()
        fm_desc[i] = fm_desc_string
        fm_explained_string = input_filelines[i][27:].strip()
        fm_explained[i] = fm_explained_string
    
    strainRate = 50  
    U_ZH = 160e3
    # only constants for 6060 and 6082 are known
    As = 0.04  
    Bs = 0.03  
    C1 = 2.0
    
    if input_datafile == "./data/6082.inp":
        As = 0.09
        Bs = 0.01
        C1 = 2.0
    
    
    
    fm.extend([strainRate, U_ZH, As, Bs, C1])
    fm_desc.extend(["strain_rate", "U_ZH", "As", "Bs", "C1"])
    fm_explained.extend(["Strain rate [1/s]", "Zener-Holomon activation energy [J/mol]", "Constant (0.04 for 6060)","Constant (0.03 for 6060)","Constant (2.0 for 6060)"])
    

    return fm, fm_desc, fm_explained



def initialize_table_values(table_data):
    """
    Initialize the table values for the simulation.

    Parameters:
    table_data (list): A list containing the table data parameters.

    Returns:
    tuple: A tuple containing the input values and their descriptions.
    """
    
    if len(table_data) == 5:
        list_elements = int(table_data[4])
    else:
        list_elements = 10
    time_list = np.logspace(np.log10(0.0001), np.log10(table_data[0]), list_elements)
    time_list = [round(number, -int(np.floor(np.log10(abs(number))))) for number in time_list]
    #time_list = np.around(time_list, decimals=3)
    #time_list = [0]*list_elements  # list of variables
    temp_list = [table_data[1]]*list_elements
    c_list = [table_data[2]]*list_elements
    pz_list = [table_data[3]]*list_elements
    
    
    
    input_desc = ['Time [s]' , 'Temperature [C]' , 'Effective concentration []' , 'PZ']
    input_values = [time_list , temp_list , c_list , pz_list] 
        
        
    return input_values, input_desc


def use_datafile(filename, input_datafile):
    """
    Replace the alloy file with the specified filename.

    Parameters:
    filename (str): The name of the file to use.
    input_datafile (str): The path to the input data file to be replaced.
    """
    
    input_filepath = "./data/" + str(filename)
    if input_filepath: 
        shutil.copyfile(input_filepath, input_datafile)       
        


