'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''
import torch
import torch.nn as nn
import time

class ALSOFT_ANN(nn.Module):
    """
    A class representing an Artificial Neural Network.
    
    Attributes:
        layers (list): A list containing the number of nodes in each layer.
        activation_func (function): The activation function to apply between layers.
    """
    
    def __init__(self, layers, activation_func):
        """
        Initialize the ANN with given layers and activation function.
        
        Args:
            layers (list): List specifying the number of nodes in each layer, including input and output.
            activation_func (torch function): Activation function for hidden layers.
        """
        
        super(ALSOFT_ANN, self).__init__()
        self.layers = layers
        self.num_layers = len(layers)
        self.fc_layers = nn.ModuleList()
        self.activation_func = activation_func
        
        # Define fully connected layers
        for i in range(self.num_layers - 1):
            self.fc_layers.append(nn.Linear(layers[i], layers[i+1]))
        
    def forward(self, x):
        """
        Forward pass through the network.
        
        Args:
            x (Tensor): Input tensor.
            
        Returns:
            Tensor: Output tensor after passing through the network.
        """
        for i in range(self.num_layers - 2):
            x = self.activation_func(self.fc_layers[i](x))
            
        x = self.fc_layers[-1](x)  # No activation function for output layer
        return x

def train_model(alsoft_ann, optimizer, num_epochs, patience, batch_size, learning_rate, 
                prepared_training_sets, alloy_indices, alsoft_ann_filename):
    """
    Train the ANN using the given parameters and data.
    
    Args:
        alsoft_ann (ALSOFT_ANN): The neural network object to train.
        optimizer (torch.Optim): The optimizer to use for training.
        num_epochs (int): Number of epochs to train.
        patience (int): Number of epochs to wait for improvement before early stopping.
        batch_size (int): Size of each training batch.
        learning_rate (float): Learning rate for the optimizer.
        prepared_training_sets (tuple): Prepared training, validation, and test sets.
        alloy_indices (list): List of indices for the alloys.
        alflow_ann_filename (str): Filename to save the trained model.
    
    Returns:
        tuple: Training time, test loss, standard deviation of residuals, residuals, end epoch, validation losses, training losses, test losses.
    """
    file_path = '../artificial_neural_networks/alsoft_neural_networks/' + alsoft_ann_filename + '.pth'
    train_losses, val_losses, test_losses = [], [], []
    inputs_train, inputs_test, inputs_val, targets_train, targets_test, targets_val = prepared_training_sets
    criterion = nn.MSELoss()
    optimizer = optimizer(alsoft_ann.parameters(), lr=learning_rate)
    best_val_loss = float('inf')
    early_stop_counter = 0
    start_time = time.time()

    for epoch in range(num_epochs):
        alsoft_ann.train()
        for i in range(0, len(inputs_train), batch_size):
            input_batch = inputs_train[i:i+batch_size]
            target_batch = targets_train[i:i+batch_size]
            labels = alsoft_ann(input_batch)
            loss = criterion(labels, target_batch)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        alsoft_ann.eval()
        with torch.no_grad():
            train_labels = alsoft_ann(inputs_train)
            train_loss = criterion(train_labels, targets_train)
            test_labels = alsoft_ann(inputs_test)
            test_loss = criterion(test_labels, targets_test)
            val_labels = alsoft_ann(inputs_val)
            val_loss = criterion(val_labels, targets_val)
        
        train_losses.append(train_loss)
        test_losses.append(test_loss)
        val_losses.append(val_loss)
        print(f'Epoch [{epoch+1}/{num_epochs}], MSE from validation-set: {val_loss.item():.2f}')
        
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            early_stop_counter = 0
            torch.save(alsoft_ann.state_dict(), file_path)
        else:
            early_stop_counter += 1
            if early_stop_counter >= patience:
                print("Early stopping triggered.")
                break
    
    end_time = time.time()
    train_time = end_time - start_time
    end_epoch = epoch + 1

    alsoft_ann.eval()
    with torch.no_grad():
        test_labels = alsoft_ann(inputs_test)
        test_loss = criterion(test_labels, targets_test)
    residuals = test_labels - targets_test
    std = torch.std(residuals).item()
    print(f"Mean Squared Error (MSE) from test-set: {test_loss.item():.2f}")
    print(f"Standard Deviation (STD) from test-set: {round(std,2)}")
    
    return train_time, test_loss, std, residuals, end_epoch, val_losses, train_losses, test_losses
    
    

