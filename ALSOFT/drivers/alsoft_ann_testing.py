'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

from alsoft_functions import running_alsoft, initialize_input, calculate_z_rho_delta, initialize_table_values
import torch
import numpy as np
import matplotlib.pyplot as plt
from alsoft_ann_training_sets import select_alloy_data
import time
import os

def epoch_loss_compare(train_losses, val_losses, test_losses, filename, hidden_layers):
    """
    Compare and plot training, validation, and test losses across epochs.

    Args:
    train_losses (list of float): List of training losses for each epoch.
    val_losses (list of float): List of validation losses for each epoch.
    test_losses (list of float): List of test losses for each epoch.
    filename (str): Filename for the ANN.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """
    
    epochs = range(1, len(train_losses) + 1)

    # Create plot
    plt.figure(figsize=(8, 6))
    plt.plot(epochs, train_losses, label='Training Loss', color='blue', alpha=0.6, linewidth=3.0)
    plt.plot(epochs, val_losses, label='Validation Loss', color='green', linestyle = "dashed", alpha = 0.8 , linewidth=2.5)
    plt.plot(epochs, test_losses, label='Test Loss', color='red', linestyle = "dotted", alpha = 0.8 , linewidth=2.0)

    plt.title('Training, Validation and Test Losses for ' + str(hidden_layers) + ' architecture')
    plt.xlabel('Epochs')
    plt.ylabel('Mean Squared Error')
    plt.xlim(1, len(train_losses))
    plt.ylim(1, 1e5)
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.yscale('log')
    
    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_epoch_compares"
    save_file = os.path.join(save_path, filename + "_epoch_compare.png")
    plt.savefig(save_file)
    plt.show()
    
    

def alsoft_ann_plot_compare(time_range, temp_range , ceff_range, alloy_indices, alsoft_ann_filename, alsoft_ann, hidden_layers):    
    """
    Plot comparison between ALFLOW results and ANN predictions.

    Args:
    time_range (list): Time range (min, max).
    temp_range (list): Temperature range (min, max).
    ceff_range (list): Effective concentration range (min, max).
    alloy_indices (list of int): List of indices representing different alloys.
    alsoft_ann_filename (str): Filename of the trained ANN model.
    alsoft_ann (torch.nn.Module): The ANN model.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """
    
    alsoft_ann_file_path = '../artificial_neural_networks/alsoft_neural_networks/' + alsoft_ann_filename + '.pth'
    
    num_plots = 3
    time_end = time_range[1]
    temp_values = np.linspace(temp_range[0], temp_range[1], num_plots)
    ceff_values = np.linspace(ceff_range[0], ceff_range[1], num_plots)    
    inputs = [[time_end, round(temp_values[i]), ceff_values[i], 4500] for i in range(num_plots)]
    
    time_matrix = []
    alsoft_rp02_matrix = []
    ann_rp02_matrix = []

    
    # Run ALSOFT and ANN
    for alloy_index in alloy_indices:
        input_datafile = select_alloy_data(alloy_index)
        fm, fm_desc, fm_explained = initialize_input(input_datafile)
        Z, rho_i , delta = calculate_z_rho_delta(fm)
        fm[48], fm[1], fm[2] = Z, rho_i , delta
        
        for i in range(len(inputs)):
            
            targets_list = []
            inputs_list = []
            alsoft_inputs, alsoft_desc = initialize_table_values(inputs[i])

            alsoft_data = running_alsoft(fm[0:49], alsoft_inputs)                      
           
            selected_rp02 = alsoft_data[11]
            selected_time = alsoft_data[0]
            
            # Creating stress torch tensor and strain tensor (NECESSARY FOR RUNNING ANN)
            for value in selected_rp02: targets_list.append(torch.tensor([value],dtype=torch.float))
            if len(alloy_indices) == 1:
                for value in selected_time: inputs_list.append(torch.tensor([value, inputs[i][1], inputs[i][2]],dtype=torch.float))
            else:
                for value in selected_time: inputs_list.append(torch.tensor([alloy_index, inputs[i][0], inputs[i][1], inputs[i][2]],dtype=torch.float))
            
            
            time_matrix.append(selected_time)
            alsoft_rp02_tensor = torch.stack(targets_list)
            alsoft_rp02_matrix.append(alsoft_rp02_tensor)
            inputs_tensor = torch.stack(inputs_list)

            alsoft_ann.load_state_dict(torch.load(alsoft_ann_file_path))  

            alsoft_ann.eval()
            with torch.no_grad():
                test_output = alsoft_ann(inputs_tensor)

            
            ann_rp02_matrix.append(test_output)
            

        
    # Create plot
    plt.figure(figsize=(8, 6))
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 12
    })
    
    colors = ['green','blue', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'purple', 'brown']
    for i in range(len(time_matrix)):
        label_ann =    'ANN:       T = '+ str(inputs[i][1]) + ' K, c_eff = ' + str(inputs[i][2])
        label_alsoft = 'ALSOFT: T = '+ str(inputs[i][1]) + ' K, c_eff = ' + str(inputs[i][2])        
        plt.plot(time_matrix[i], alsoft_rp02_matrix[i], label=label_alsoft, color=colors[i], alpha=1, linewidth=2.0)
        plt.plot(time_matrix[i], ann_rp02_matrix[i], label=label_ann, color=colors[i], linestyle='dotted', alpha=0.4, linewidth=2.0)
   
    plt.xscale("log")
    plt.xlabel('Time [s]')
    plt.ylabel('Rp02')
    plt.title('Test plot')
    plt.title('Comparison between ALSOFT results and ANN predictions for ' + str(hidden_layers) + ' architecture')
    plt.legend(loc='lower left')
    
    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_plot_compares"
    save_file = os.path.join(save_path, alsoft_ann_filename[26:] + "_plot_compare.png")
    plt.savefig(save_file)
    
    plt.show()

def error_histogram(residuals, ann_filename, hidden_layers):
    """
    Plot a histogram of ANN prediction errors.

    Args:
    residuals (torch.Tensor): Tensor of prediction errors.
    ann_filename (str): Filename of the trained ANN model.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """

    array = residuals.numpy()
    hist, bins = np.histogram(array, bins=100,  range=(-50, 50))  
    
    # Create histogram plot
    plt.figure(figsize=(8, 6))
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 12
    })
    plt.hist(array, bins=bins, alpha=0.7, color='blue', edgecolor='black')
    
    plt.xlabel('Error [MPa]')
    plt.ylabel('Instances')
    plt.title('ANN Error Histogram for ' + str(hidden_layers) + ' architecture')
    plt.grid(True)

    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_histograms"
    save_file = os.path.join(save_path, ann_filename + "_histogram.png")
    plt.savefig(save_file)
    
    plt.show()
    
def alsoft_ann_timer(alsoft_ann_filename, alsoft_ann):
    """
    Measure and compare the execution time of the ANN against ALFLOW.

    Args:
    alsoft_ann_filename (str): Filename of the trained ANN model.
    alsoft_ann (torch.nn.Module): The ANN model.

    Returns:
    average_divided (float): Average execution time per prediction of 1500 strain points.
    """
    
    alsoft_ann_file_path = '../artificial_neural_networks/alsoft_neural_networks/' + alsoft_ann_filename + '.pth'

    alsoft_points = 770
    multiplier_ratio = 1000 
    inputs = [1.2, 523 , 0.0543]
    inputs_list = []
    random_time = np.linspace(0,10, alsoft_points*multiplier_ratio)
    inputs_list = np.column_stack((random_time, np.full_like(random_time, inputs[1]), np.full_like(random_time, inputs[2])))
    inputs_tensor = torch.tensor(inputs_list, dtype=torch.float)
    ann_times = []
    alsoft_ann.load_state_dict(torch.load(alsoft_ann_file_path))
    
    for i in range(300):

        # Set the alsoft_ann to evaluation mode
        alsoft_ann.eval()
        ann_start_time = time.perf_counter()
        with torch.no_grad():
            alsoft_ann(inputs_tensor)
        ann_end_time = time.perf_counter()
        ann_time = ann_end_time - ann_start_time
        ann_times.append(ann_time)
    
    average_alsoft_time = 0.000899
    ann_times = sorted(ann_times)[:-200] # to get rid of the biggest outliers
    std = np.std(np.array(ann_times)/multiplier_ratio)
    average_ann_time = sum(ann_times)/len(ann_times)
    average_divided = average_ann_time/multiplier_ratio
    alflow_ann_ratio = average_alsoft_time/average_divided
    
    print(f"Average ANN time: {average_divided*1000000} microseconds")
    print(f"ANN is {alflow_ann_ratio:.2f} times faster than ALFLOW")
    print(f"Standard deviation of ANN time: {std*1000000} microseconds")
    
    return average_divided


def create_plots(time_range, temp_range, ceff_range, alloy_indices, alsoft_ann_filename, alsoft_ann, residuals):
    
    """
    For the Jupityr Notebook. Create and display plots comparing ALSOFT results with ANN predictions, along with an error histogram.

    Args:
    time_range (tuple): Time range as (min, max) in seconds.
    temp_range (tuple): Temperature range as (min, max).
    ceff_range (tuple): Effective temperature range as (min, max).
    alloy_indices (list of int): List of indices representing different alloys.
    alsoft_ann_filename (str): Filename of the trained ANN model (without file extension).
    alsoft_ann (torch.nn.Module): The ANN model used for predictions.
    residuals (tensor): Tensor containing the prediction errors (residuals).
    """

    
    alsoft_ann_file_path = '../artificial_neural_networks/alsoft_neural_networks/' + alsoft_ann_filename + '.pth'
    
    num_plots = 3
    time_end = time_range[1]
    temp_values = np.linspace(temp_range[0], temp_range[1], num_plots)
    ceff_values = np.linspace(ceff_range[0], ceff_range[1], num_plots)    
    inputs = [[time_end, round(temp_values[i]), ceff_values[i], 4500] for i in range(num_plots)]
    
    time_matrix = []
    alsoft_rp02_matrix = []
    ann_rp02_matrix = []

    
    # Run ALSOFT and ANN
    for alloy_index in alloy_indices:
        input_datafile = select_alloy_data(alloy_index)
        fm, fm_desc, fm_explained = initialize_input(input_datafile)
        Z, rho_i , delta = calculate_z_rho_delta(fm)
        fm[48], fm[1], fm[2] = Z, rho_i , delta
        
        for i in range(len(inputs)):
            
            targets_list = []
            inputs_list = []
            alsoft_inputs, alsoft_desc = initialize_table_values(inputs[i])

            alsoft_data = running_alsoft(fm[0:49], alsoft_inputs)                      
           
            selected_rp02 = alsoft_data[11]
            selected_time = alsoft_data[0]
            
            # Creating stress torch tensor and strain tensor (NECESSARY FOR RUNNING ANN)
            for value in selected_rp02: targets_list.append(torch.tensor([value],dtype=torch.float))
            if len(alloy_indices) == 1:
                for value in selected_time: inputs_list.append(torch.tensor([value, inputs[i][1], inputs[i][2]],dtype=torch.float))
            else:
                for value in selected_time: inputs_list.append(torch.tensor([alloy_index, inputs[i][0], inputs[i][1], inputs[i][2]],dtype=torch.float))
            
            
            time_matrix.append(selected_time)
            alsoft_rp02_tensor = torch.stack(targets_list)
            alsoft_rp02_matrix.append(alsoft_rp02_tensor)
            inputs_tensor = torch.stack(inputs_list)

            alsoft_ann.load_state_dict(torch.load(alsoft_ann_file_path))  

            alsoft_ann.eval()
            with torch.no_grad():
                test_output = alsoft_ann(inputs_tensor)

            
            ann_rp02_matrix.append(test_output)
            

    array = residuals.numpy()
    hist, bins = np.histogram(array, bins=100,  range=(-50, 50))
        
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    colors = ['green','blue', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'purple', 'brown']
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 10
    })

    
    # First subplot - Histogram
    ax1.hist(array, bins=bins, alpha=0.7, color='blue', edgecolor='black')
    ax1.set_xlabel('Error [MPa]')
    ax1.set_ylabel('Instances')
    ax1.set_title('ANN Error Histogram', fontsize=12)
    ax1.grid(True)
    
    # Second subplot - Line plots
    for i in range(3):
        label_ann =    'ANN:       T = '+ str(inputs[i][1]) + ' K, c_eff = ' + str(inputs[i][2])
        label_alsoft = 'ALSOFT: T = '+ str(inputs[i][1]) + ' K, c_eff = ' + str(inputs[i][2])  
        ax2.plot(time_matrix[i], alsoft_rp02_matrix[i], label=label_alsoft, color=colors[i], alpha=0.4, linewidth=2.0)
        ax2.plot(time_matrix[i], ann_rp02_matrix[i], label=label_ann, color=colors[i], linestyle='dotted', alpha=1, linewidth=2.0)
    
    ax2.set_xscale("log")
    ax2.set_xlabel('Time [s]')
    ax2.set_ylabel('Rp02')
    ax2.set_title('Comparison between ALSOFT results and ANN predictions', fontsize=12)
    ax2.grid(True)
    ax2.legend(loc='lower left')

    plt.tight_layout()
    plt.show()
    

