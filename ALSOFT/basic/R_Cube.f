C  $Id: R_Cube.f 207 2006-09-24 08:18:11Z friisj $
C  Description:   Fraction of cube
C  Project:       VIRFAB
C  Developed by:  NTNU IME, N-7465 Trondheim, Norway
C  Author:        Shahriar Abtahi
C  Date:          08 Jabuary 2001
C
C------------------------------------------------------------------------------

      double precision function R_Cube(Z, R_c, strain)
      implicit none

C# External variables ---------------------------------------------------------
      include 'data.inc'
      double precision   Z, R_c, strain

C==============================================================================
      R_Cube = R_c*R_cA*(R_cB+R_cC*strain)**(R_cD)*
     &         (R_cE*log(Z)+R_cF)**(R_cG)

c      print *,'R_cA = ',R_cA
c      print *,'R_cB = ',R_cB
c      print *,'R_cC = ',R_cC
c      print *,'R_cD = ',R_cD
c      print *,'R_cE = ',R_cE      
c      print *,'R_cF = ',R_cF
c      print *,'R_cG = ',R_cG
c      print *,'Z    = ',Z
c      print *,'R_c0 = ',R_c0

      return
      end
