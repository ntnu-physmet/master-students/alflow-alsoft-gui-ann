      double precision function eta_c(rho_i,delta,T,PZ)
      implicit none

C# External variables ---------------------------------------------------------
      include 'data.inc'
      double precision   rho_i, delta, T, PZ

C# External functions ---------------------------------------------------------
      double precision   P_D
      external           P_D

C# Internal varibales
      double precision PD

C==============================================================================

      PD = P_D(T,delta,rho_i)
      eta_c=4.*gamma_GB/(PD - PZ)

c     print *,' gamma_GB = ',gamma_GB
c     print *,' P_D      = ',PD
c     print *,' P_Z      = ',PZ
c     print *,' T        = ',T
c     print *,' delta    = ',delta
c     print *,' rho_i    = ',rho_i
  
      return
      end
