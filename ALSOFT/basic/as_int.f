      subroutine as_int(rho_i, delta, r, output_data, out_L,
     $     time1, time2, T1, T2, Css1, Css2, PZ1, PZ2, 
     $     timestep)
      implicit none
      double precision rho_i, delta, r,
     $     time1, time2, T1, T2, Css1, Css2, PZ1, PZ2, 
     $     timestep
      double precision, dimension(23,3000) :: output_data
	  integer out_L
C-----------------------------------------------------------------------
C     $Id: as_int.f 538 2007-05-16 09:19:03Z friisj $
C     Daniel Preminger 25-1-2024
C
C     DESCRIPTION:
C     Integrate the evolution equations from time1 to time2. The other
C     external parameters are linearly interpolated over the time
C     interval. Output is written to unit uout at the end of each
C     subinterval of length timestep.
C
C     Initial values can be written by calling this function with time2
C     equal to time1.
C
C     ARGUMENTS:
C       rho_i     (In/Out)   Internal dislocation density (/m^2)
C       delta     (In/Out)   Subgrain size (m)
C       r         (In/Out)   Mean grain size (m)
C       output_data (In/Out) Output data array
C       out_L         (in/out)   Output line
C       time1     (In)       Initial time (s)
C       time2     (In)       Final   time (s)
C       T1        (In)       Initial temperature (K)
C       T2        (In)       Final   temperature (K)
C       Css1      (In)       Initial solute content (at. fraction)
C       Css2      (In)       Final   solute content (at. fraction)
C       PZ1       (In)       Initial Zener drag (Pa)
C       PZ2       (In)       Final   Zener drag (Pa)
C       timestep  (In)       Length of subinterval for output (s). If set 
C                            to zero, output will only be written at the 
C                            end of the time interval.  
C-----------------------------------------------------------------------

C  -- Local variables
      integer neq, lrw, liw
      parameter(neq=3, lrw=22+16*neq+2*neq**2, liw=30+neq)
      double precision y(neq), time, tout, rtol, atol(neq), rwork(lrw)
      integer itol, itask, istate, iopt, iwork(liw), mf
      double precision rpar(8)
      integer ipar(1)

      double precision dy(neq)
      integer i

      external as_fun, as_jac

      double precision max_timestep
      external         max_timestep

C  -- Assign array of state variables
      y(1) = rho_i
      y(2) = delta
      y(3) = r

C  -- Assign arrays of external parameters
      rpar(1)  = time1
      rpar(2)  = time2
      rpar(3)  = T1
      rpar(4)  = T2
      rpar(5)  = Css1
      rpar(6)  = Css2
      rpar(7)  = PZ1
      rpar(8)  = PZ2

C  -- Initiate time
      time = time1



C  -- Only print at end of interval if timestep is zero
      if (timestep.eq.0) timestep = time2 - time1


C  -- Method of integration:
C       mf     10=normal, 22=stiff, internally generated Jacobian
C       itask  1=normal integration, 2=single step, 4=do not overshoot rwork(1)
C       istate set to 1 before first call
      mf       = 10
      itask    = 1
      istate   = 1

C  -- Integration tolerances:
      itol    = 2
      rtol    = 1.d-5
      atol(1) = 1.d6
      atol(2) = 1.d-12
      atol(3) = 1.d-12

C  -- Optional input:
C   - iopt: 0=no optional input, 1=use non-zero optional input
      iopt = 0
      do i = 5,10
         rwork(i) = 0.0d0
         iwork(i) = 0
      enddo
C   - max time to compute (for itask=4,5)
c      rwork(1) = time2
C   - length of initial time step
c      rwork(5) = 1.0
C   - maximal allowed steplength
      rwork(6) = max_timestep()
C   - minimal allowed steplength
c      rwork(7) = 1.0E-11
C   - lower half-bandwidth for banded systems
c      iwork(1) = 1
C   - upper half-bandwidth for banded systems
c      iwork(2) = 1
C   - maximum order to be allowed
c      iwork(5) = 12
C   - maximum number of steps during a call to the solver
c      iwork(6) = 500
C   - maximum number of messages printed
c      iwork(7) = 10



C  -- Start integration of each time step
      tout = time2 - 1.0

      do while (tout.lt.time2)
         tout = time + timestep
         if (tout.gt.time2) tout = time2

C        For some reason we need to re-initialise DVODE before every call(!)
c         istate = 1

         call dvode(as_fun,neq,y,time,tout,itol,rtol,atol,itask,
     $        istate,iopt,rwork,lrw,iwork,liw,as_jac,mf,rpar,ipar)

C        Write parameter values at current time to file
         call as_fun(neq, time, y, dy, rpar, ipar)
		 out_L = out_L+1
         call as_write(output_data, out_L)

C        Print messages and stop on error
         if (istate.lt.0) then
            stop
         endif

      enddo

C  -- Assign state variables on output
      rho_i = y(1)
      delta = y(2)
      r     = y(3)
         
      return
      end



c----------------------------------------------------------------------
c----------------------------------------------------------------------




      double precision function max_timestep()
      implicit none
C     Returns the maximal length of a time step. We use that rho_i never
C     must be negative during one time step as a criterion.
      include 'fun.inc'
      max_timestep = 0.01*rho_i/rho_idot
      return
      end



c dummy routine
c--------------------------------------------------------------------

      subroutine as_jac (neq, t, y, ml, mu, pd, nrpd, rpar, ipar)
      implicit none
      integer neq, ipar, ml, mu, nrpd
      double precision pd, rpar, t, y
      dimension y(neq), pd(nrpd,neq)
C     Dummy subroutine for the time integrator.  The Jacobian is
C     calculated numerical, since an analytical Jacobian is not yet
C     implemented.
c      pd(i,j) = df(i)/dy(j)
      return
      end


      


