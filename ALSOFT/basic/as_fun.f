      subroutine as_fun(neq, time, y, dy, rpar, ipar)
      implicit none
      integer neq, ipar(1)
      double precision time, y(neq), dy(neq), rpar(8)
C-----------------------------------------------------------------------
C     $Id: as_fun.f 698 2007-12-17 18:55:02Z friisj $
C
C     Called by DVODE. This subroutine defines the system by
C     calculating the time derivertives of the state parameters.
C
C     ARGUMENTS:
C       neq      (In)      Number of equations (= number of state parameters)
C       time     (In)      Time (s)
C       y(neq)   (In)      Current value of state parameters
C       dy(neq)  (Out)     Calculated time derivatives of state parameters
C       rpar(*)  (In)      External parameters (real)
C       ipar(*)  (In)      External parameters (integer)
C
C     STATE PARAMETERS:
C       rho_i  = y(1)      Internal dislocation density (/m^2)
C       delta  = y(2)      Subgrain size (m)
C       r      = y(3)      Mean grain size (m)
C     
C     EXTERNAL PARAMETERS:
C       time1 = rpar(1)    
C       time2 = rpar(2)
C       T1    = rpar(3)    Temperature (K)
C       T2    = rpar(4)
C       Css1  = rpar(5)    Effective solute concentration (at. fraction)
C       Css2  = rpar(6)
C       PZ1   = rpar(7)    Zener drag (Pa)
C       PZ2   = rpar(8)
C
C-----------------------------------------------------------------------

C  -- Include common block with input data
      include 'data.inc'

C  -- Include common block with parameters calculated here
      include 'fun.inc'

C  -- External functions
      double precision P_D, f_G
      external         P_D, f_G
c      double precision N_PSN, N_Cube, N_GB
c      external         N_PSN, N_Cube, N_GB
      
C  -- Local parameters
      double precision f

      integer nwarn
      save nwarn
      data nwarn /0/

C-----------------------------------------------------------------------

C  -- Remember when this function was called last, needed by as_write()
      calltime = time

C  -- Give state parameters understandable names
      rho_i = y(1)
      delta = y(2)
      r     = y(3)

C  -- Do a linear interpolation of external parameters
      f = (time - rpar(1))/(rpar(2) - rpar(1))
      if (rpar(1).eq.rpar(2)) f = 0.0
      T   = (1.0 - f)*rpar(3)  + f*rpar(4)
      Css = (1.0 - f)*rpar(5)  + f*rpar(6)
      PZ  = (1.0 - f)*rpar(7)  + f*rpar(8)

C  -- Calculating driving force PD and check that it is larger than PZ
      PD = P_D(T, delta ,rho_i)
      if (PD.lt.PZ+1) then
         if (nwarn.lt.1) print *, 'WARNING: At time =',calltime,
     $        ': PD =', PD, ' becomes less than PZ + 1 Pa =', PZ+1
         PD = PZ+1
         nwarn = nwarn + 1
      endif

C  -- Other needed quantities
      Mobility = M0/(Css*Rgas*T) * exp(-U_Rex/(Rgas*T))
      Growth   = Mobility*(PD - Pz)

      G        = f_G(T)

      A_rho    = w_rho   * Css**(-e_rho)
      A_delta  = w_delta * Css**(-e_delta)

C  -- Calculate time derivatives
      rho_idot = -2.0*v_D*b*B_rho*A_rho*rho_i**(1.5)*exp(-U_a/(Rgas*T))*
     $        sinh(A_rho*G*b*b*b*b*sqrt(rho_i)/(kB*T))
      deltadot = 2.*v_D*b*B_delta*A_delta*exp(-U_a/(Rgas*T))*
     $        sinh(A_delta*G*b*b*b*b/(delta*kB*T))
      rdot = Growth

C-------------------------

C  -- Assign output array
      dy(1) = rho_idot
      dy(2) = deltadot
      dy(3) = rdot

      return
      end
