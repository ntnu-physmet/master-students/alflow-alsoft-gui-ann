C  $Id: S_Cube.f 207 2006-09-24 08:18:11Z friisj $
C  Description:   Number of subgrains per unit volume larger than delta*
C  Project:       VIRFAB
C  Developed by:  NTNU IME, N-7465 Trondheim, Norway
C  Author:        Shahriar Abtahi
C  Date:          09 Jabuary 2001
C
C------------------------------------------------------------------------------

      double precision function S_Cube(deltaCritical, deltaMean)
      implicit none

C# External variables ---------------------------------------------------------
      double precision  deltaCritical, deltaMean

C# Internal variables ---------------------------------------------------------
      double precision  sum, fact, term, A, integral
      integer i

C==============================================================================
      A = deltaCritical/deltaMean

      if((A .lt. 0.).or.(A .gt. 15.)) then
         S_Cube = 0.
         return
      end if

      sum  = 1.
      fact = 1.

C# Integrate the "f" function -------------------------------------------------
      A = 5.*deltaCritical/deltaMean
      do i=1,4
         fact = fact*i
         term = A**i/fact
         sum  = sum + term
      end do
      integral = exp(-A)*sum

      S_Cube = integral / deltaMean**3

      return
      end
