      subroutine as_calc(X, D, Rp02)
      implicit none
      double precision X, D, Rp02
C-----------------------------------------------------------------------
C     $Id: as_calc.f 563 2007-05-24 21:35:52Z friisj $
C
C     DESCRIPTION 
C     Calculate the value of the output arguments based on the state at
C     last call to as_fun().
C
C     ARGUMENTS
C       X      (Out)   Volume fraction recrystalised
C       D      (Out)   Mean size of recrystalised grains (m)
C       Rp02   (Out)   Flow stress (Pa)
C
C-----------------------------------------------------------------------
      include 'data.inc'
      include 'fun.inc'
      double precision Xext

      Xext = 4.0/3.0*pi*r**3 * Ntot
      X    = 1.0 - exp(-Xext)

      D    = (X/Ntot)**(1.0/3.0)

      Rp02 = R_FLP + (1.0-X)*M*G*b*(alpha1*sqrt(rho_i) +
     $     alpha2/delta)

      return
      end
