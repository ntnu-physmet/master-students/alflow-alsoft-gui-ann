      subroutine as_write(output_data, out_L)
      implicit none
      integer out_L
	  double precision, dimension(23,3000) :: output_data
C-----------------------------------------------------------------------
C     $Id: as_write.f 800 2009-07-30 14:38:05Z friisj $
C
C     DESCRIPTION:
C     Write parameters to unit uout calculated by the last call
C     as_fun().  A negative unit number will print a header. 
C     out_L = output_element (in)
C-----------------------------------------------------------------------
      double precision Xext, X, D, Rp02

      include 'data.inc'
      include 'fun.inc'

C     $        '#        time(s)',
C     $        'rho_i(1/m^2)','delta(m)','r(m)',
C     $        'T(C)','Css','PZ(Pa)',
C     $        'PD(Pa)','Mobility(m/sPa)','Growth(m/s)',
C     $        'G(Pa)','Rp02(Pa)',
C     $        'X','Xext','D(m)',
C     $        'Z(1/s)','strain_def',
C     $        'Ntot(1/m^3)','Ncube(1/m^3)','Ngb(1/m^3)','Npsn(1/m^3)',
C     $        'fCube','fGB','fPSN'

      call as_calc(X, D, Rp02)
      Xext = 4.0/3.0*pi*r**3 * Ntot

      output_data(1,out_L) = calltime
      output_data(2,out_L) = rho_i
      output_data(3,out_L) = delta
      output_data(4,out_L) = r
      output_data(5,out_L) = T-273.15
      output_data(6,out_L) = Css
      output_data(7,out_L) = PZ
      output_data(8,out_L) = PD
      output_data(9,out_L) = Mobility
      output_data(10,out_L) = Growth
      output_data(11,out_L) = G
      output_data(12,out_L) = Rp02
      output_data(13,out_L) = X
      output_data(14,out_L) = Xext
      output_data(15,out_L) = D
      output_data(16,out_L) = ZenHol
      output_data(17,out_L) = strain_def
      output_data(18,out_L) = Ntot
      output_data(19,out_L) = Ncube
      output_data(20,out_L) = Npsn
      output_data(21,out_L) = Ncube/Ntot
      output_data(22,out_L) = Ngb/Ntot
      output_data(23,out_L) = Npsn/Ntot
      return
      end
