C  $Id: R_s.f 207 2006-09-24 08:18:11Z friisj $
C  Description:   Fraction of S in the material
C  Project:       VIRFAB
C  Developed by:  NTNU IME, N-7465 Trondheim, Norway
C  Author:        Shahriar Abtahi
C  Date:          08 Jabuary 2001
C
C------------------------------------------------------------------------------

      double precision function R_s(strain)
      implicit none

C# External variables ---------------------------------------------------------
      include 'data.inc'
      real *8  strain

C==============================================================================
      if(strain .lt. R_sC) then
        R_s= R_sA + R_sB*strain
      else
        R_s= R_sA + R_sB*R_sC
      endif

      return
      end
