      double precision function f_G(T)
      implicit none
      double precision T
C-----------------------------------------------------------------------
C     $Id: f_G.f 530 2007-05-05 13:05:19Z friisj $
C     Shear modulus
C-----------------------------------------------------------------------
      include 'data.inc'
c      f_G = 2.99e10*Dexp(-5.4e-4*T)
c      f_G = 2.65d10
      f_G = G0*Dexp(-G1*T)
      end
