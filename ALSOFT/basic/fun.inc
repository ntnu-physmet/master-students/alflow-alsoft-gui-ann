C     -*- Mode: fortran -*-

C-----------------------------------------------------------------------
C     $Id: fun.inc 507 2007-04-27 18:25:24Z friisj $
C
C     We put parameters calculated in pa_fun() that might be of
C     interest in other functions.
C-----------------------------------------------------------------------

C  -- State parameters
      double precision    rho_i, delta, r
      common/as_statepar/ rho_i, delta, r

C  -- Calculated time derivatives
      double precision     rho_idot, deltadot, rdot
      common/as_Dstatepar/ rho_idot, deltadot, rdot

C  -- External parameters
      double precision  calltime, T, Css, PZ
      common/as_extpar/ calltime, T, Css, PZ


C  -- Calculated parameters
      double precision 
     $     A_rho, A_delta,
     $     PD, Mobility, Growth, G
c     $     Ncube, Ngb, Npsn, Ntot,
c     $     X_ext, D_Max
      common/as_funpar/
     $     A_rho, A_delta,
     $     PD, Mobility, Growth, G
c     $     Ncube, Ngb, Npsn, Ntot,
c     $     X_ext, D_Max

