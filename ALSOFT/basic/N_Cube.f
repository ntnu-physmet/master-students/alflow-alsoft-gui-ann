      double precision function N_Cube(rho_i,delta,T,PZ,strain,Z)
      implicit none
      double precision   rho_i, delta, T, PZ, strain, Z
C-----------------------------------------------------------------------
C     $Id: N_Cube.f 537 2007-05-16 00:37:22Z friisj $
C
C     Returns the density of cube sites.
C-----------------------------------------------------------------------
      include 'data.inc'

      double precision   deltaMean, Rc, deltaCritical
      double precision   Rs, Sc

      double precision   R_Cube, R_s, S_Cube, P_D, eta_c
      external           R_Cube, R_s, S_Cube, P_D, eta_c

      Rc = R_Cube(Z, R_c0, strain)

      deltaCritical = eta_c(rho_i, delta, T, PZ)
      deltaMean     = delta*fCube

      Rs            = R_s(strain)
      Sc            = S_Cube(deltaCritical, deltaMean)

      N_Cube= 2.0*C_Cube*deltaMean*Rc*(1.0-Rc)*Rs*Sc/D0 *
     &        (exp(strain) + exp(-strain)+ 1.0)

c       print *, ' C_Cube = ', C_cube
c       print *, ' deltaMean = ', deltaMean
c       print *, ' Rs = ', Rs  
c       print *, ' Rc = ', Rc 
c       print *, ' S_Cube = ', Sc
c       print *, ' deltaCritical = ', deltaCritical
c       print *, ' D0 = ', D0
c       print *, ' N_Cube = ', N_Cube


      return
      end
