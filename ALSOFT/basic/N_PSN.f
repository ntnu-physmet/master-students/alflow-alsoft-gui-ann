      double precision function N_PSN(rho_i,delta,T,PZ,strain,Z)
      implicit none
      double precision   rho_i, delta, T, PZ, strain, Z
C-----------------------------------------------------------------------
C     $Id: N_PSN.f 537 2007-05-16 00:37:22Z friisj $
C
C     Returns the density of Particle Stimulated Nucleation sites.
C-----------------------------------------------------------------------
      include 'data.inc'

      double precision   etac

      double precision   P_D, eta_c
      external           P_D, eta_c

      etac = eta_c(rho_i, delta, T, PZ)

      if (etac .lt. 0.) then
         N_PSN = 0.
      elseif (L*etac .gt. 30.) then
         N_PSN = 0.
      else
         N_PSN = C_PSN*N0*exp(-L*CPE*etac)
      endif


c      print *,' Npsn = ', N_PSN
c      print *,'eta_c = ', etac
c      print *,'C_PSN = ', C_PSN     
c      print *,'N0    = ', N0    
c      print *,'L     = ', L
c      print *,'CPE   = ', CPE

      return
      end
