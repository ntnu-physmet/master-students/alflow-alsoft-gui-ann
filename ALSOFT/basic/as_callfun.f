      subroutine as_callfun(rho_i, delta, r, time, T, Css, PZ)
      implicit none
      double precision rho_i, delta, r, time, T, Css, PZ
C-----------------------------------------------------------------------
C     $Id: as_callfun.f 509 2007-04-27 23:02:21Z friisj $
C
C     DESCRIPTION 
C     Convinients function for calling as_fun(), which takes explicit
C     values of the state and external parameters.
C-----------------------------------------------------------------------
      integer neq, nrpar, nipar
      parameter (neq=3, nrpar=8, nipar=1)
      double precision y(neq), dy(neq), rpar(nrpar)
      integer ipar(nipar)

C  -- Assign array of state variables
      y(1) = rho_i
      y(2) = delta
      y(3) = r

C  -- Assign arrays of external parameters
      rpar(1)  = time
      rpar(2)  = time
      rpar(3)  = T
      rpar(4)  = T
      rpar(5)  = Css
      rpar(6)  = Css
      rpar(7)  = PZ
      rpar(8)  = PZ

C  -- Call as_fun()
      call as_fun(neq, time, y, dy, rpar, ipar)

      return
      end
