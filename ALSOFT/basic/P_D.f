C  $Id: P_D.f 207 2006-09-24 08:18:11Z friisj $
C  Description:   Calculate the driving pressure for recrystallization
C  Project:       
C  Developed by:  NTNU IME, N-7465 Trondheim, Norway
C  Author:        Knut Marthinsen
C  Date:          25 May 2004
C
C------------------------------------------------------------------------------
      double precision function P_D(T,delta,rho_i)
      implicit none

C# External variables ---------------------------------------------------------
      include 'data.inc'
      double precision   T, delta, rho_i

C# External functions ---------------------------------------------------------
      double precision   f_G
      external f_G

C# Internal variables ---------------------------------------------------------
      double precision   G, gamma_SB 

C==============================================================================
c      G = 2.99e10 * exp(-5.4e-4 * T)
c      G = 2.65D10
      G = f_G(T)
     
      gamma_SB= G*b*theta/(4.*Pi*(1.-nu))*(log(e*theta_c/theta))
c     C1      = 2.
c     rho     = C1*C1/(delta**2)

c      print *,' gamma_SB = ', gamma_SB
c      print *,' delta    = ', delta
c      print *,' rho_i    = ', rho_i
c      print *,' alpha    = ', alpha
      
      P_D = alpha*gamma_SB/delta+0.5*G*b*b*rho_i

      return
      end
