      subroutine as_init(input_data, rho_i, delta, r)
      implicit none
      double precision rho_i, delta, r
	  double precision , dimension(49) :: input_data
C-----------------------------------------------------------------------
C     $Id: as_init.f 563 2007-05-24 21:35:52Z friisj $
C
C     Initiates Alsoft
C
C     Reads input parameters from unit uin.  The initial values for the
C     state variables are returned.
C
C     ARGUMENTS:
C     input_data (In)     Input array
C     rho_i    (Out)    Initial internal dislocation density (m^-2)
C     delta    (Out)    Initial subgrain size (m)
C     r        (Out)    Initial mean grain size (m)
C
C-----------------------------------------------------------------------
      include 'data.inc'
      double precision N_Cube, N_GB, N_PSN
      external         N_Cube, N_GB, N_PSN

C  -- Assign constants
      pi    = 2.*Asin(1.d0)
      e     = exp(1.d0)
      Rgas  = NA*kB

C  -- Read input to common data
      as_mode = int(input_data(1))

      rho_i = input_data(2)
      delta = input_data(3)
      r = input_data(4)

      R_FLP = input_data(5)
      B_rho = input_data(6)
      w_rho = input_data(7)
      B_delta = input_data(8)
      w_delta = input_data(9)

      N0 = input_data(10)
      L = input_data(11)
      M0 = input_data(12)
      D0 = input_data(13)

      e_delta = input_data(14)
      e_rho = input_data(15)
      alpha1 = input_data(16)
      alpha2 = input_data(17)
      M = input_data(18)
      b = input_data(19)
      v_D = input_data(20)

      gamma_GB = input_data(21)
      CPE = input_data(22)
      C_PSN = input_data(23)
      C_GB = input_data(24)
      alpha = input_data(25)
      theta = input_data(26)
      theta_c = input_data(27)
      nu = input_data(28)

      R_c0 = input_data(29)
      C_cube = input_data(30)
      fCube = input_data(31)

      R_cA = input_data(32)
      R_cB = input_data(33)
      R_cC = input_data(34)
      R_cD = input_data(35)
      R_cE = input_data(36)
      R_cF = input_data(37)
      R_cG = input_data(38)
      R_sA = input_data(39)
      R_sB = input_data(40)
      R_sC = input_data(41)

      U_a = input_data(42)
      U_rex = input_data(43)

      G0 = input_data(44)
      G1 = input_data(45)

      T_def = input_data(46)
      PZ_def = input_data(47)
      strain_def = input_data(48)
      ZenHol = input_data(49)

C  -- Correct units
      T_def = T_def + 273.15
      R_FLP = R_FLP*1.e6
      
      theta   = theta   *Pi/180.0
      theta_c = theta_c *Pi/180.0

C  -- Calculate number of nuclei (site saturation)
      Ncube = N_Cube(rho_i, delta, T_def, PZ_def, strain_def, ZenHol)
      Ngb   = N_GB(  rho_i, delta, T_def, PZ_def, strain_def, ZenHol)
      Npsn  = N_PSN( rho_i, delta, T_def, PZ_def, strain_def, ZenHol)
      Ntot  = Ncube + Ngb + Npsn
      
      return
      end
