      double precision function N_GB(rho_i,delta,T,PZ,strain,Z)
      implicit none
      double precision   rho_i, delta, T, PZ, strain, Z
C-----------------------------------------------------------------------
C     $Id: N_GB.f 537 2007-05-16 00:37:22Z friisj $
C
C     Returns the density of grain boundary sites.
C-----------------------------------------------------------------------
      include 'data.inc'

      double precision   deltaMean, deltaCritical, S_GB, Rc

      double precision   R_Cube, S_Cube, P_D, eta_c
      external           R_Cube, S_Cube, P_D, eta_c

      Rc = R_Cube(Z, R_c0, strain)
c     Rc = 0.

      deltaCritical = eta_c(rho_i, delta, T, PZ)
      deltaMean     = delta

      S_GB = S_Cube(deltaCritical, deltaMean)

      N_GB = 2.* C_GB*deltaMean*(1.0-Rc)*S_GB / D0 *
     &      (exp(strain) + exp(-strain)+ 1.0)

c     print *,' Rc, C_GB, strain =', Rc, C_GB, strain 
c     print *,' T, delta, rho_i =', T, delta, rho_i 
c     print *,' deltaCritical, deltaMeanS_GB = ', deltaCritical, 
c    &      deltaMean, S_GB
c     print * 

      return
      end
