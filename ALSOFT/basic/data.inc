C     -*- Mode: fortran -*-

C-----------------------------------------------------------------------
C     $Id: data.inc 563 2007-05-24 21:35:52Z friisj $
C     Parameters and common blocks included in the sources.
C-----------------------------------------------------------------------

C  -- Constants. pi, e and Rgas are calculated in as_init()
      double precision NA, kB
      parameter(NA=6.02214199D23, kB=1.3806503D-23)
      double precision pi, Rgas,e
      common/as_const/ pi, Rgas,e


C  -- Input parameters
      double precision
     $     B_delta, B_rho, v_D, U_a, b, 
     $     M, alpha1, alpha2, U_rex, R_FLP, 
     $     theta, theta_c, nu, gamma_GB,
     $     C_PSN, N0, CPE, L, M0, D0, C_GB, alpha,
     $     e_delta, e_rho, 
     $     R_c0, C_Cube, fCube,
     $     R_cA,R_cB,R_cC,R_cD,R_cE,R_cF,R_cG,R_sA,R_sB,R_sC, 
     $     w_rho, w_delta,
     $     G0, G1,
     $     T_def, PZ_def, strain_def, ZenHol
      integer as_mode

      common /as_data/ 
     $     B_delta, B_rho, v_D, U_a, b, 
     $     M, alpha1, alpha2, U_rex, R_FLP, 
     $     theta, theta_c, nu, gamma_GB,
     $     C_PSN, N0, CPE, L, M0, D0, C_GB, alpha,
     $     e_delta, e_rho, 
     $     R_c0, C_Cube, fCube,
     $     R_cA,R_cB,R_cC,R_cD,R_cE,R_cF,R_cG,R_sA,R_sB,R_sC, 
     $     w_rho, w_delta,
     $     G0, G1, 
     $     T_def, PZ_def, strain_def, ZenHol,
     $     as_mode


C -- Initially calculated quantities
      double precision    Ntot, Ncube, Ngb, Npsn
      common /as_initpar/ Ntot, Ncube, Ngb, Npsn



C     Removed
c     $     FTh,
c     $     c, c_ss0, c0, 
c     $     As, Bs, C_1,
c     $     C_pz, 
c     $     Rp02, f_rho
c     $     strainRate0, T_def, U_ZH
c     $     f0, kappa, U_GB
