
	  
	  Subroutine alflow_v6(input_data,extra_param,output_data)

	  implicit none
	  
	  integer L, Nout
      double precision  phi, rhoi, delta, rho
      double precision tau_a, tau_t, tau_p, tau
      double precision rho_m, conc_sc
      double precision dgamma, G, M_Taylor
      double precision  gamma0,gamma1, T0,T1, gamdot0,gamdot1, D0,D1
      double precision rhoi_eff
      double precision dtau, tau0
c  -- local declarations from input
      double precision Na, tmp1
	  
c  -- common blocks

      double precision , dimension(1501) :: gamdot_array 
	  double precision , dimension(1501) :: strain_array
	  double precision , dimension(1501) :: M_Taylor_array
	  double precision , dimension(1501) :: T_array
	  double precision , dimension(1501) :: D_array

      include "../basic/cmnprm.f"

	  double precision , dimension(62) :: input_data
Cf2py intent(in) :: input_data
	  double precision , dimension(5,1501) :: extra_param
Cf2py intent(in) :: extra_param 
      double precision, dimension(12,1500) :: output_data
Cf2py intent(out) :: output_data 


c    -- nout must be the same as output_data parameter
      Nout = 1500
	  
	  T_array(1:1501) = extra_param(1, 1:1501)
	  gamdot_array(1:1501) = extra_param(2, 1:1501)
	  strain_array(1:1501) = extra_param(3, 1:1501)
	  M_Taylor_array(1:1501) = extra_param(4, 1:1501)
	  D_array(1:1501) = extra_param(5, 1:1501)

c  What kind of recovery to be allowed?
c  calculate RC_mode (between 1 and 7) using the following formula:
c      RC_mode = 4*pm+2*sg+sc,  where  
c  pure metal recovery  :  pm=1 if allowed, else 0.
c  solute glide recovery:  sg=1 if allowed, else 0.
c  solute climb recovery:  sc=1 if allowed, else 0.

c  	RC_mode = int(input_data(1))

      RC_mode = input_data(1)
C  Material and alloy specific data
      nu_Debye = input_data(2)
      b = input_data(3)

c    Boltzmann
      k = input_data(4)
c     Poisson ratio (Elastic)
      nu = input_data(5)
C  ALLOYING
      conc = input_data(6)
      conc_0 = input_data(7)
      n_sc = input_data(8)
      dU_sc = input_data(9)
      B_sc = input_data(10)
c  SCALING parameters
      q_c = input_data(11)
      C = input_data(12)
c     Volume fraction of cell-walls in stage II
      f = input_data(13)
c  Parameters relating microstructure and flow stress
      alpha1 = input_data(14)
      alpha2 = input_data(15)
c  relating subgrain-size to total dislocation density
      kappa_0 = input_data(16)
C  Activation energies , remember particle or mass
      U_t_pm = input_data(17)
      U_t_s = input_data(18)
      U_SD = input_data(19)
      U_s = input_data(20)
      dU_S = input_data(21)
C     per particle
      Na      = 6.022e+23
      U_t_pm  = U_t_pm/Na
      U_t_s   = U_t_s/Na
      U_SD    = U_SD/Na
      U_s     = U_s/Na
      dU_s    = dU_s/Na
      dU_sc   = dU_sc/Na

      kappa_2 = input_data(22)

      A      = input_data(23)
      phi_c = input_data(24)
C
C  Thermal tau constants
      B_t_pm = input_data(25)
      B_t_s = input_data(26)
      omega_rho = input_data(27)
      omega_t = input_data(28)
      e_t = input_data(29)
c     rho_m = m * rho
      m = input_data(30)

c     rhoi recovery constants
      B_rho_pm = input_data(31)
      B_rho_sg = input_data(32)
      B_rho_sc = input_data(33)

      ksi_rho_pm = input_data(34)
      ksi_rho_sg = input_data(35)
      ksi_rho_sc = input_data(36)

      omega_s = input_data(37)
      e_rho = input_data(38)

c     delta recovery constants
      B_delta_pm = input_data(39)
      B_delta_s = input_data(40)
      ksi_delta_pm = input_data(41)
      ksi_delta_s = input_data(42)
c     new parameter in v1.02
      e_delta = input_data(43)

c     read S_IV/S_sc
      S_IV  = input_data(44)


c     parameters, misorientation
      q_IV = input_data(45)
      phi_IV = input_data(46)

C-----------------------------------
C---  the initial conditions --------

      rhoi = input_data(47)
      D0 = input_data(48)
	  D0 = D0 * 1.0E-6

c     New parameters added in order to take into account 
c     solute of low diffusivity at RT
      
      tau_cl  = input_data(49)
      T_cl  = input_data(50)
      dT_cl  = input_data(51)
      q_b = input_data(52)

c-----------------------------------
c     strength contribution from precipitates
      tau_prec = input_data(53)

c-----------------------------------
c     slip length factors, volume fractions and radii for non-sherable 
c     particle classes
      kappa3_prec = input_data(54)
      f_prec = input_data(55)
      r_prec  = input_data(56)
      kappa3_disp  = input_data(57)
      f_disp = input_data(58)
      r_disp = input_data(59)
      kappa3_prim = input_data(60)
      f_prim = input_data(61)
      r_prim = input_data(62)

C---  Parameters that in the future should be added to the input file ---

c     New parameters for the grain size effect. For now, we just
c     set them to default values that will turn off the grain size 
c     effect...
c      delta0 = 1
c      delta_III = 1e-6
c      kappa_4 = 0


c     q_b from q_c ,alpha1, alpha2 and f if q_b < 1.
      if(q_b.lt.1) then
         tmp1   = alpha2/(alpha1*q_c)
         q_b = sqrt(tmp1*(tmp1+2.)/f +1.)
c         write(*,*) "input q_b less than 1, set to: ", q_b 
      endif

c     Scaling constants S in delta equation (decrease term)
      S_sc   = f*q_b**2./(2.*(C**2.)*(1-f))

c     Stage II scaling parameter
      S_II   = f*(q_b**2-1.)/(2.* C**2)

C     calculates S_IV from S_sc
      S_IV   = S_IV*S_sc

c     delta and phi default initial conditions
      delta = q_c/Sqrt(rhoi)
      phi   = b*q_c*f*(q_b**2-1.)*sqrt(rhoi)/kappa_0



	 


c     STRAIN step and Number of POINTS:

      gamma0  = 0.
      gamma1 = 0.
c  -- declared earlier :      Nout   = 1500

      tau0 = -1e12

c  -- Start of strain loop ___________________________________________

      Do L = 1, Nout
	     
		 T0 = T_array(L)
		 T1 = T_array(L+1)
		 M_Taylor = M_Taylor_array(L)
         gamdot0 = gamdot_array(L)*M_Taylor
		 gamdot1 = gamdot_array(L+1)*M_Taylor
		 D0 = D_array(L)
		 D1 = D_array(L+1)

         gamma0 = strain_array(L)*M_Taylor
         gamma1 = strain_array(L+1)*M_Taylor
		 dgamma = gamma1 - gamma0
		

c  -- Integration of the microstructure from gamma0 to gamma1
         
         Call AF_strain(rhoi, delta, phi, gamma0, gamma1, 
     &         T0, T1, gamdot0, gamdot1, D0, D1)


c  -- Calculate further outputs at gamma1

         rho     = rhoi + kappa_0*phi/(b*delta)
         rho_m   = m*rho
         call G_subAF(G,T1)
         Call smooth_rhoi(rhoi_eff, rhoi, delta)
         Call stress_a(tau_a, rhoi_eff, delta, phi,D1,G,T1)
         call stress_p(tau_p, G)
         call stress_t(tau_t,rho, gamdot1,G,T1,rho_m)
         call conc_AF(conc_sc,rho_m, T1, gamdot1)
         tau = tau_a + tau_p + tau_t        

c  -- For Kocks-Mecking diagram
         dtau = tau - tau0
         tau0 = tau


        output_data(1,L) = 1.0E-6*M_Taylor*tau_a
        output_data(2,L) = gamma1/M_Taylor
        output_data(3,L) = 1.0E-6*M_Taylor*tau
        output_data(4,L) = rhoi
        output_data(5,L) = delta
        output_data(6,L) = rho
        output_data(7,L) = phi
        output_data(8,L) = gamdot1/M_Taylor
        output_data(9,L) = conc_sc
        output_data(10,L) = delta/(q_c/sqrt(rhoi))
        output_data(11,L) = tau_t
        output_data(12,L) = 1.0e-6*M_Taylor**2 * dtau/dgamma
                  
      EndDo      

c  -- End of strain loop______________________________________________

	  RETURN
	  
      End
	  
 