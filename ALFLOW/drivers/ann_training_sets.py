'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

from alflow_functions import running_alflow, initialize_input, expand_list
from sklearn.model_selection import train_test_split
import torch
import numpy as np


def prepare_training_sets(inputs_tensor, targets_tensor, test_size=0.15, val_size=0.15, random_state=42):
    """
    Split the input and target tensors into training, validation, and test sets.
    
    Args:
    inputs_tensor (torch.Tensor): Tensor containing the input data.
    targets_tensor (torch.Tensor): Tensor containing the target data.
    test_size (float, optional): Proportion of the data to include in the test set (default is 0.15).
    val_size (float, optional): Proportion of the data to include in the validation set (default is 0.15).
    random_state (int, optional): Seed for random number generation (default is 42).
    
    Returns:
    tuple: Contains the training, test, and validation sets for inputs and targets.
    """
    
    inputs_train, inputs_temp, targets_train, targets_temp = train_test_split(inputs_tensor, targets_tensor, test_size=test_size + val_size, random_state=random_state)
    
    # Split temporary sets into testing and validation sets
    inputs_test, inputs_val, targets_test, targets_val = train_test_split(inputs_temp, targets_temp, test_size=val_size/(test_size + val_size), random_state=random_state)
    
    return inputs_train, inputs_test, inputs_val, targets_train, targets_test, targets_val

def set_alflow_inputs(input_data):
    """
    Create a list of inputs for the ALFLOW simulation.
    
    Args:
    input_data (list): List containing the input parameters [temperature, strain rate, strain end, grain size and Taylor factor].
    
    Returns:
    list: List of input parameters expanded for ALFLOW simulation.
    """
    input_list_alflow = [expand_list([input_data[i]],1501) for i in range(0,5)]
    input_list_alflow[2] = expand_list([0,input_data[2]],1501)
    return input_list_alflow


def save_training_sets(inputs_tensor, targets_tensor, inputs_filename, targets_filename):
    """
    Save the input and target tensors to files.
    
    Args:
    inputs_tensor (torch.Tensor): Tensor containing the input data.
    targets_tensor (torch.Tensor): Tensor containing the target data.
    inputs_filename (str): Filename for saving the input tensor.
    targets_filename (str): Filename for saving the target tensor.
    """
    
    inputs_file_path = '../artificial_neural_networks/ann_inputs_targets/' + inputs_filename
    targets_file_path = '../artificial_neural_networks/ann_inputs_targets/' + targets_filename
    torch.save(inputs_tensor, inputs_file_path)
    torch.save(targets_tensor, targets_file_path)

                   
def load_training_sets(inputs_filename, targets_filename):
    """
    Load the input and target tensors from files.

    Args:
    inputs_filename (str): Filename for loading the input tensor.
    targets_filename (str): Filename for loading the target tensor.

    Returns:
    tuple: Contains the input and target tensors.
    """
    
    inputs_file_path = '../artificial_neural_networks/ann_inputs_targets/' + inputs_filename
    targets_file_path = '../artificial_neural_networks/ann_inputs_targets/' + targets_filename
    inputs_tensor = torch.load(inputs_file_path)
    targets_tensor = torch.load(targets_file_path)
    return inputs_tensor, targets_tensor


def select_alloy_data(index):
    """
    Select the data file for a given alloy index.

    Args:
    index (int): Index of the alloy.

    Returns:
    str: Filepath corresponding to the selected alloy index.
    """
    
    filenames = ['', 'AA1050', 'AA5182' , 'Mg1', 'Mg1~', 'Mg3' , 'Mg3~' , 'Mg05' , 'Mg05~']
    indexed_filename = './data/prmv301.' + filenames[index] 
    return indexed_filename

def create_training_inputs(input_lengths , input_ranges):
    """
    Create arrays of input parameters for training.

    Args:
    input_lengths (list of int): List of desired lengths for each input parameter array.
    input_ranges (list of tuple): List of (min, max) ranges for each desired input parameter.

    Returns:
    list: List of arrays for each input parameter.
    """
    
    t0_array = np.linspace(input_ranges[0][0],input_ranges[0][1],input_lengths[0]) 
    gamdot0_array = np.logspace(np.log10(input_ranges[1][0]), np.log10(input_ranges[1][1]), input_lengths[1])
    strain_end_array = np.linspace(input_ranges[2][0],input_ranges[2][1],input_lengths[2])
    return [t0_array , gamdot0_array ,strain_end_array]

def create_training_sets(alloy_indices, inputs):
    """
    Create training sets by running ALFLOW simulations and collecting input and target data.

    Args:
    alloy_indices (list of int): List of indices representing different alloys.
    inputs (list of arrays): List of arrays for each input parameter.

    Returns:
    tuple: Contains the input tensor and target tensor.
    """
    
    targets_list = []
    inputs_list = []
    for alloy_index in alloy_indices:
        input_datafile = select_alloy_data(alloy_index)
        fm, fm_desc, fm_explained = initialize_input(input_datafile)
        strain_elements = len(inputs[2])
        for i in range(len(inputs[0])):
            # print(str(i+1) + "/" + str(len(inputs[0])))
            for j in range(len(inputs[1])):
                input_var = [inputs[0][i], inputs[1][j], inputs[2][-1], 3, 50]
                alflow_inputs = set_alflow_inputs(input_var)
                data = running_alflow(fm, alflow_inputs)
                i_points = 1500//strain_elements
                
                selected_stress = data[2][j%i_points::i_points] # modulo to get different stresses
                for value in selected_stress: targets_list.append(torch.tensor([value],dtype=torch.float))
                selected_strain = data[1][j%i_points::i_points]
                if len(alloy_indices) == 1:
                    for value in selected_strain: inputs_list.append(torch.tensor([inputs[0][i], inputs[1][j], value],dtype=torch.float))
                else: 
                    for value in selected_strain: inputs_list.append(torch.tensor([alloy_index, inputs[0][i], inputs[1][j], value],dtype=torch.float))
    targets_tensor = torch.stack(targets_list)
    inputs_tensor = torch.stack(inputs_list)
    
    return inputs_tensor, targets_tensor




def make_training_sets(alloy_indices, inputs_filename, targets_filename, input_lengths, input_ranges):
    """
    Generate and save training sets for the specified alloys and input ranges.

    Args:
    alloy_indices (list of int): List of indices representing different alloys.
    inputs_filename (str): Filename for saving the input tensor.
    targets_filename (str): Filename for saving the target tensor.
    input_lengths (list of int): List of lengths for each input parameter array.
    input_ranges (list of tuple): List of (min, max) ranges for each input parameter.

    Returns:
    int: Number of data points in the training set.
    """
    
    print("Producing training sets for ranges in temperatures [K]: " + str(input_ranges[0]) 
          +", strain rates [1/s]: " +  str(input_ranges[1]) + " and strains: " + str(input_ranges[2]))
    inputs = create_training_inputs(input_lengths, input_ranges)
    inputs_tensor, targets_tensor = create_training_sets(alloy_indices, inputs)
    save_training_sets(inputs_tensor, targets_tensor, inputs_filename, targets_filename)
    data_points = len(targets_tensor)
    print("Training data containing: " + str(data_points) + " elements, saved to " 
          + inputs_filename + " and "  + targets_filename)
    return data_points

