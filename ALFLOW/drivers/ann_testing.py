'''
Author: Daniel Preminger
Date: 07.06.2024

As part of a master's thesis at the Department of Materials Science at NTNU.
'''

from ann_training_sets import set_alflow_inputs , select_alloy_data
from alflow_functions import running_alflow, initialize_input
import torch
import numpy as np
import matplotlib.pyplot as plt
import time
import os

def epoch_loss_compare(train_losses, val_losses, test_losses, filename, hidden_layers):
    """
    Compare and plot training, validation, and test losses across epochs.

    Args:
    train_losses (list of float): List of training losses for each epoch.
    val_losses (list of float): List of validation losses for each epoch.
    test_losses (list of float): List of test losses for each epoch.
    filename (str): Filename for the ANN.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """
    
    epochs = range(1, len(train_losses) + 1)
    
    # Create plot
    plt.figure(figsize=(8, 6))
    plt.plot(epochs, train_losses, label='Training Loss', color='blue', alpha=0.6, linewidth=3.0)
    plt.plot(epochs, val_losses, label='Validation Loss', color='green', linestyle = "dashed", alpha = 0.8 , linewidth=2.5)
    plt.plot(epochs, test_losses, label='Test Loss', color='red', linestyle = "dotted", alpha = 0.8 , linewidth=2.0)

    plt.title('Training, Validation and Test Losses for ' + str(hidden_layers) + ' architecture')
    plt.xlabel('Epochs')
    plt.ylabel('Mean Squared Error')
    plt.xlim(1, len(train_losses))
    plt.ylim(1, 1e5)
    plt.legend(loc='upper right')
    plt.grid(True)
    plt.yscale('log')
    
    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_epoch_compares"
    save_file = os.path.join(save_path, filename + "_epoch_compare.png")
    plt.savefig(save_file)
    plt.show()
    
    

def alflow_ann_plot_compare(temp_range, strainrate_range, strain_range, alloy_indices, ann_filename, alflow_ann, hidden_layers):
    """
    Plot comparison between ALFLOW results and ANN predictions.

    Args:
    temp_range (list): Temperature range (min, max).
    strainrate_range (list): Strain rate range (min, max).
    strain_range (list): Strain range (min, max).
    alloy_indices (list of int): List of indices representing different alloys.
    ann_filename (str): Filename of the trained ANN model.
    alflow_ann (torch.nn.Module): The ANN model.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """
    
    ann_file_path = '../artificial_neural_networks/neural_networks/' + ann_filename + '.pth'
    
    
    num_plots = 3
    temp_values = np.linspace(temp_range[0] + 0.01 * (temp_range[1] - temp_range[0]), temp_range[1] - 0.01 * (temp_range[1] - temp_range[0]), num_plots)
    strainrate_values = np.logspace(np.log10(strainrate_range[0]), np.log10(strainrate_range[1]), num_plots)
    strain_value = strain_range[1]
    inputs = [[round(temp_values[i]), strainrate_values[i], strain_value] for i in range(num_plots)]
    
    strain_matrix = []
    alflow_stress_matrix = []
    ann_stress_matrix = []

    # Run ALFLOW and ANN
    for alloy_index in alloy_indices:
        input_datafile = select_alloy_data(alloy_index)
        fm, fm_desc, fm_explained = initialize_input(input_datafile)
        for i in range(len(inputs)):
            stress_list = []
            inputs_list = []
            
            input_var = inputs[i] + [3, 50]  # needed for ALFLOW 
            alflow_inputs = set_alflow_inputs(input_var)
            
            alflow_data = running_alflow(fm, alflow_inputs)

            
            selected_stress = alflow_data[2] 
            selected_strain = alflow_data[1]
            
            # Creating stress torch tensor and strain tensor (NECESSARY FOR RUNNING ANN)
            for value in selected_stress: stress_list.append(torch.tensor([value],dtype=torch.float))
            if len(alloy_indices) == 1:
                for value in selected_strain: inputs_list.append(torch.tensor([inputs[i][0], inputs[i][1], value],dtype=torch.float))
            else:
                for value in selected_strain: inputs_list.append(torch.tensor([alloy_index, inputs[i][0], inputs[i][1], value],dtype=torch.float))
            
            
            strain_matrix.append(selected_strain)
            alflow_stress_tensor = torch.stack(stress_list)
            alflow_stress_matrix.append(alflow_stress_tensor)
            inputs_tensor = torch.stack(inputs_list)
            
            alflow_ann.load_state_dict(torch.load(ann_file_path))  

            alflow_ann.eval()
            with torch.no_grad():
                test_output = alflow_ann(inputs_tensor)
            
            ann_stress_matrix.append(test_output)
        
    # Create plot
    plt.figure(figsize=(8, 6))
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 12
    })
    
    colors = ['green','blue', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'purple', 'brown']
    for i in range(3):
        label_ann =    'ANN:         T = '+ str(inputs[i][0]) + ' K, \u03B5\u0307 = ' + str(inputs[i][1])
        label_alflow = 'ALFLOW: T = '+ str(inputs[i][0]) + ' K, \u03B5\u0307 = ' + str(inputs[i][1])
        plt.plot(strain_matrix[i], alflow_stress_matrix[i], label=label_alflow, color=colors[i], alpha=0.4, linewidth=2.0)
        plt.plot(strain_matrix[i], ann_stress_matrix[i], label=label_ann, color=colors[i], linestyle='dotted', alpha=1, linewidth=2.0)
    plt.grid(True)
    plt.xlim(0,5)
    plt.ylim(0,400)
    plt.xlabel('Strain [-]')
    plt.ylabel('Stress [MPa]')
    plt.title('Comparison between ALFLOW results and ANN predictions for ' + str(hidden_layers) + ' architecture')
    plt.legend(loc='upper left')
    
    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_plot_compares"
    os.makedirs(save_path, exist_ok=True)  # Create the directory if it doesn't exist
    save_file = os.path.join(save_path, ann_filename + "_plot_compare.png")
    plt.savefig(save_file)
    
    plt.show()
    

def error_histogram(residuals, ann_filename, hidden_layers):
    """
    Plot a histogram of ANN prediction errors.

    Args:
    residuals (torch.Tensor): Tensor of prediction errors.
    ann_filename (str): Filename of the trained ANN model.
    hidden_layers (int): Number of hidden layers in the ANN architecture.
    """
    
    array = residuals.numpy()
    hist, bins = np.histogram(array, bins=100,  range=(-50, 50))  
    
    # Create histogram plot
    plt.figure(figsize=(8, 6))
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 12
    })
    plt.hist(array, bins=bins, alpha=0.7, color='blue', edgecolor='black')
    
    plt.xlabel('Error [MPa]')
    plt.ylabel('Instances')
    plt.title('ANN Error Histogram for ' + str(hidden_layers) + ' architecture')
    plt.grid(True)
    
    # Save plot to adjacent folder
    save_path = "../artificial_neural_networks/ann_histograms"
    save_file = os.path.join(save_path, ann_filename + "_histogram.png")
    plt.savefig(save_file)
    
    plt.show()
    
def ann_timer(ann_filename, alflow_ann):
    """
    Measure and compare the execution time of the ANN against ALFLOW.

    Args:
    ann_filename (str): Filename of the trained ANN model.
    alflow_ann (torch.nn.Module): The ANN model.

    Returns:
    average_divided (float): Average execution time per prediction of 1500 strain points.
    """
    
    
    
    ann_file_path = '../artificial_neural_networks/neural_networks/' + ann_filename + '.pth'

    
    alflow_points = 1500
    multiplier_ratio = 1000 
    inputs = [573, 0.25 , 5]
    inputs_list = []
    random_strain = np.linspace(0,5, alflow_points*multiplier_ratio)
    inputs_list = np.column_stack((np.full_like(random_strain, inputs[0]), np.full_like(random_strain, inputs[1]), random_strain))
    inputs_tensor = torch.tensor(inputs_list, dtype=torch.float)
    ann_times = []
    alflow_ann.load_state_dict(torch.load(ann_file_path))
    
    for i in range(300):

        # Set the alflow_ann to evaluation mode
        alflow_ann.eval()
        
        ann_start_time = time.perf_counter()
        with torch.no_grad():
            alflow_ann(inputs_tensor)
        ann_end_time = time.perf_counter()
        ann_time = ann_end_time - ann_start_time
        ann_times.append(ann_time)
    
    average_alflow_time = 0.0216
    ann_times = sorted(ann_times)[:-200] # to get rid of the biggest outliers
    std = np.std(np.array(ann_times)/multiplier_ratio)
    average_ann_time = sum(ann_times)/len(ann_times)
    average_divided = average_ann_time/multiplier_ratio
    alflow_ann_ratio = average_alflow_time/average_divided
    
    print(f"Average ANN time: {average_divided*1000000} microseconds")
    print(f"ANN is {alflow_ann_ratio:.2f} times faster than ALFLOW")
    print(f"Standard deviation of ANN time: {std*1000000} microseconds")
    
    return average_divided


def create_plots(temp_range, strainrate_range, strain_range, alloy_indices, ann_filename, alflow_ann ,residuals):
    """
    For the Jupityr Notebook. Create and display plots comparing ALFLOW results with ANN predictions, along with an error histogram.

    Args:
    temp_range (tuple): Temperature range as (min, max) in Kelvin.
    strainrate_range (tuple): Strain rate range as (min, max).
    strain_range (tuple): Strain range as (min, max).
    alloy_indices (list of int): List of indices representing different alloys.
    ann_filename (str): Filename of the trained ANN model (without file extension).
    alflow_ann (torch.nn.Module): The ANN model used for predictions.
    residuals (tensor): Tensor containing the prediction errors (residuals).
    """
    
    ann_file_path = '../artificial_neural_networks/neural_networks/' + ann_filename
    
    num_plots = 3
    temp_values = np.linspace(temp_range[0] + 0.05 * (temp_range[1] - temp_range[0]), temp_range[1] - 0.05 * (temp_range[1] - temp_range[0]), num_plots)
    strainrate_values = np.logspace(np.log10(strainrate_range[0]), np.log10(strainrate_range[1]), num_plots)
    strain_value = strain_range[1]
    inputs = [[round(temp_values[i]), strainrate_values[i], strain_value] for i in range(num_plots)]

    
        
    strain_matrix = []
    alflow_stress_matrix = []
    ann_stress_matrix = []

    
    # For each alloy
    for alloy_index in alloy_indices:
        input_datafile = select_alloy_data(alloy_index)
        fm, fm_desc, fm_explained = initialize_input(input_datafile)
        for i in range(len(inputs)):
            stress_list = []
            inputs_list = []
            
            input_var = inputs[i] + [3, 50]  # needed for alflow 
            alflow_inputs = set_alflow_inputs(input_var)
            alflow_data = running_alflow(fm, alflow_inputs)

            
            
            
            selected_stress = alflow_data[2] 
            selected_strain = alflow_data[1]
            
            # Creating stress torch tensor (maybe not necessary) and strain tensor (NECESSARY FOR RUNNING ANN)
            for value in selected_stress: stress_list.append(torch.tensor([value],dtype=torch.float))
            if len(alloy_indices) == 1:
                for value in selected_strain: inputs_list.append(torch.tensor([inputs[i][0], inputs[i][1], value],dtype=torch.float))
            else:
                for value in selected_strain: inputs_list.append(torch.tensor([alloy_index, inputs[i][0], inputs[i][1], value],dtype=torch.float))
            
            
            strain_matrix.append(selected_strain)
            alflow_stress_tensor = torch.stack(stress_list)
            alflow_stress_matrix.append(alflow_stress_tensor)
            inputs_tensor = torch.stack(inputs_list)
            
            alflow_ann.load_state_dict(torch.load(ann_file_path))  
            
            alflow_ann.eval()
            with torch.no_grad():
                test_output = alflow_ann(inputs_tensor)
            ann_stress_matrix.append(test_output)
        
    
    # Use NumPy histogram
    array = residuals.numpy()
    hist, bins = np.histogram(array, bins=100,  range=(-50, 50))  
    
    # Create a figure and two subplots side by side
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    colors = ['green','blue', 'red', 'cyan', 'magenta', 'yellow', 'black', 'orange', 'purple', 'brown']
    plt.rcParams.update({
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
        'font.size': 10
    })


    ax1.hist(array, bins=bins, alpha=0.7, color='blue', edgecolor='black')
    ax1.set_xlabel('Error [MPa]')
    ax1.set_ylabel('Instances')
    ax1.set_title('ANN Error Histogram', fontsize=12)
    ax1.grid(True)
    

    for i in range(3):
        label_ann = f'ANN: T = {inputs[i][0]} K, \u03B5\u0307 = {inputs[i][1]}'
        label_alflow = f'ALFLOW: T = {inputs[i][0]} K, \u03B5\u0307 = {inputs[i][1]}'
        ax2.plot(strain_matrix[i], alflow_stress_matrix[i], label=label_alflow, color=colors[i], alpha=0.4, linewidth=2.0)
        ax2.plot(strain_matrix[i], ann_stress_matrix[i], label=label_ann, color=colors[i], linestyle='dotted', alpha=1, linewidth=2.0)
    
    ax2.set_xlim(0, 5)
    ax2.set_ylim(0, 400)
    ax2.set_xlabel('Strain [-]')
    ax2.set_ylabel('Stress [MPa]')
    ax2.set_title('Comparison between ALFLOW results and ANN predictions', fontsize=12)
    ax2.grid(True)
    ax2.legend(loc='upper left')

    plt.tight_layout()
    plt.show()