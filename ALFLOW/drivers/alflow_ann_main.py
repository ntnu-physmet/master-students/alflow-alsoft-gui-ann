"""
Author: Daniel Preminger
Date: 07.06.2024
As part of a master's thesis at the Department of Materials Science at NTNU.
"""

from ann_model_training import train_model, ALFLOW_ANN
from ann_training_sets import make_training_sets, load_training_sets, prepare_training_sets
from ann_testing import alflow_ann_plot_compare, error_histogram, epoch_loss_compare, ann_timer
import torch  
from datetime import datetime
import openpyxl

def create_filename(alloy_indices, types):
    """
    Create a filename based on the current date, alloy indices, and type.
    
    Args:
        alloy_indices (list): List of alloy indices.
        types (str): Type of file, either "ann", "inputs" or "targets".
        
    Returns:
        str: Generated filename, without file path or extension (added before loading or saving).
    """
    
    today_date = datetime.now()
    formatted_date = today_date.strftime("%m_%d_%H_%M")
    alloy_list = ['', 'AA1050', 'AA5182', 'Mg1', 'Mg1~', 'Mg3', 'Mg3~', 'Mg05', 'Mg05~']
    filename = 'ann_' + formatted_date
    for index in alloy_indices:
        filename += "_" + alloy_list[index]
    if types == "inputs":
        filename += "_inputs.pt"
    elif types == "targets":
        filename += "_targets.pt"
    return filename

def create_model(inputs_filename, targets_filename, alloy_indices, hidden_layers, optimizer, activation_func,
                 num_epochs, patience, batch_size, learning_rate, temp_range, strainrate_range, strain_range):
    """
    Create, train, and evaluate the ANN model. 
    Model evaluation is saved to an Excel sheet, and plots are saved to adjacent folders.
    
    Args:
        inputs_filename (str): Filename for input data.
        targets_filename (str): Filename for target data.
        alloy_indices (list): List of alloy indices.
        hidden_layers (list): List of hidden layers.
        optimizer (torch.optim function): Optimizer for training.
        activation_func (torch function): Activation function for hidden layers.
        num_epochs (int): Number of epochs for training.
        patience (int): Number of epochs to wait for improvement before early stopping.
        batch_size (int): Batch size for training.
        learning_rate (float): Learning rate for the optimizer.
        temp_range (list): Temperature range (for plotting).
        strainrate_range (list): Strain rate range (for plotting).
        strain_range (list): Strain range (for plotting).
    """
    
    file_path = '../artificial_neural_networks/Neural_Network.xlsx'
    today_date = datetime.now()
    formatted_date = today_date.strftime("%m_%d")
    alflow_ann_filename = create_filename(alloy_indices, "ann")
    
    # Adding input and output layers
    layers_main = hidden_layers.copy()
    input_size = 3 if len(alloy_indices) == 1 else 4
    output_size = 1
    layers_main.insert(0, input_size)
    layers_main.append(output_size) 
    
    #Loading and preparing training sets
    inputs_tensor, targets_tensor = load_training_sets(inputs_filename, targets_filename)
    prepared_training_sets = prepare_training_sets(inputs_tensor, targets_tensor, 
                                                   0.15, 0.15, random_state=int(str(datetime.now())[-3:]))
    # Creating Artificial Neural Network, and training it.
    alflow_ann = ALFLOW_ANN(layers_main, activation_func)
    train_time, test_loss, std, residuals, end_epoch, val_losses, train_losses, test_losses = train_model(
        alflow_ann, optimizer, num_epochs, patience, batch_size, learning_rate, prepared_training_sets, 
        alloy_indices, alflow_ann_filename)
    
    
    # Evaluating ANN based on computational time, and creating comparative plots, histogram, and epoch evolution plot.
    alflow_time = ann_timer(alflow_ann_filename, alflow_ann)
    alflow_ann_plot_compare(temp_range, strainrate_range, strain_range, alloy_indices, 
                            alflow_ann_filename, alflow_ann, hidden_layers)
    error_histogram(residuals, alflow_ann_filename, hidden_layers)
    epoch_loss_compare(train_losses, val_losses, test_losses, alflow_ann_filename, hidden_layers)
    
    # Locating training data information in "Training data" Excel sheet.
    workbook = openpyxl.load_workbook(file_path)
    sheet = workbook["Training data"]
    row = 1
    while sheet[f'C{row}'].value != inputs_filename:
        row += 1
    total_points = sheet[f'I{row}'].value
    
    # Locating first empty row of "Neural Network" Excel sheet.
    sheet = workbook["Neural Networks"]
    empty_row = 1
    while sheet[f'A{empty_row}'].value is not None and sheet[f'C{empty_row}'].value != alflow_ann_filename:
        empty_row += 1
        
    # Writing data to the first empty row
    sheet[f'A{empty_row}'] = empty_row - 1
    sheet[f'B{empty_row}'] = formatted_date
    sheet[f'C{empty_row}'] = alflow_ann_filename
    sheet[f'D{empty_row}'] = len(hidden_layers)
    sheet[f'E{empty_row}'] = str(hidden_layers)
    sheet[f'F{empty_row}'] = round(test_loss.item(), 2)
    sheet[f'G{empty_row}'] = round(std, 2)
    sheet[f'H{empty_row}'] = round(train_time)
    sheet[f'I{empty_row}'] = str(alloy_indices)
    sheet[f'J{empty_row}'] = total_points
    sheet[f'K{empty_row}'] = str(activation_func)
    sheet[f'L{empty_row}'] = str(optimizer)
    sheet[f'M{empty_row}'] = learning_rate
    sheet[f'N{empty_row}'] = num_epochs
    sheet[f'O{empty_row}'] = end_epoch
    sheet[f'P{empty_row}'] = batch_size
    sheet[f'Q{empty_row}'] = alflow_time
    sheet[f'R{empty_row}'] = row - 1
    
    workbook.save(file_path)
    
def create_training_data(alloy_indices, inputs_filename, targets_filename, input_lengths, input_ranges):
    """
    Create training data and save to specified file paths.
    
    Args:
        alloy_indices (list): List of alloy indices.
        inputs_filename (str): Filename for input data.
        targets_filename (str): Filename for target data.
        input_lengths (list): List specifying the number of data points for each input.
        input_ranges (list): List of ranges for each input.
    """
    
    file_path = '../artificial_neural_networks/Neural_Network.xlsx'
    today_date = datetime.now()
    formatted_date = today_date.strftime("%m_%d")
    
    # Creating training data from ALFLOW
    data_points = make_training_sets(alloy_indices, inputs_filename, targets_filename, 
                                     input_lengths, input_ranges)

    # Locating first empty row of "Training Data" Excel sheet.
    workbook = openpyxl.load_workbook(file_path)
    sheet = workbook["Training data"]
    empty_row = 1
    while sheet[f'A{empty_row}'].value is not None and sheet[f'C{empty_row}'].value != inputs_filename:
        empty_row += 1
        
    # Writing data to the first empty row
    sheet[f'A{empty_row}'] = empty_row - 1
    sheet[f'B{empty_row}'] = formatted_date
    sheet[f'C{empty_row}'] = inputs_filename
    sheet[f'D{empty_row}'] = targets_filename
    sheet[f'E{empty_row}'] = str(alloy_indices)
    sheet[f'F{empty_row}'] = input_lengths[0]
    sheet[f'G{empty_row}'] = input_lengths[1]
    sheet[f'H{empty_row}'] = input_lengths[2]
    sheet[f'I{empty_row}'] = data_points
    sheet[f'J{empty_row}'] = input_ranges[0][0]
    sheet[f'K{empty_row}'] = input_ranges[0][1]
    sheet[f'L{empty_row}'] = input_ranges[1][0]
    sheet[f'M{empty_row}'] = input_ranges[1][1]
    sheet[f'N{empty_row}'] = input_ranges[2][1]
    
    workbook.save(file_path)

# Alloy indices and input specifications
alloy_indices = [3]  # ['', 'AA1050', 'AA5182', 'Mg1', 'Mg1~', 'Mg3', 'Mg3~', 'Mg05', 'Mg05~']
t0_items = 30
t0_range = [273, 773]
gamdot0_items = 30
gamdot0_range = [0.001, 10]
strain_items = 125
strain_range = [0, 5]
input_lengths = [t0_items, gamdot0_items, strain_items]
input_ranges = [t0_range, gamdot0_range, strain_range]

inputs_filename = create_filename(alloy_indices, "inputs")
targets_filename = create_filename(alloy_indices, "targets")
create_training_data(alloy_indices, inputs_filename, targets_filename, input_lengths, input_ranges)

# Training parameters
num_epochs = 1000
patience = num_epochs / 4
batch_size = 50
hidden_layers_array = [[40, 20]]
activation_func = torch.relu
optimizer = torch.optim.Adam
learning_rate = 0.001

# Train the model for each hidden layer configuration
for i, hidden_layers in enumerate(hidden_layers_array):
    print(f"Hidden layers: {hidden_layers}")
    print(f"{i+1} / {len(hidden_layers_array)}")
    create_model(inputs_filename, targets_filename, alloy_indices, hidden_layers, 
                 optimizer, activation_func, num_epochs, patience, batch_size, 
                 learning_rate, t0_range, gamdot0_range, strain_range)

    
