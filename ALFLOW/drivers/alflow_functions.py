'''
Author: Daniel Preminger
Date: 20.12.2023

As part of a specialization project at the Department of Materials Science at NTNU.
'''

import alflow_v6                          # importing the fortran extension module
import shutil                             # for copying file
import numpy as np                        

def running_alflow(fm ,input_var):
    """
    Uses the Fortran extension module for ALFLOW computations.

    Parameters:
        fm (list): The 62 parameter list in the format [p0, p1, p2, ..., p61].
        input_var (list): A list of 5 lists, each containing 1501 data points for 
                          strain, strain-rate, temperature, grain size, and Taylor factor.

    Returns:
        data: The result of the ALFLOW computation.
    """
    data = alflow_v6.alflow_v6(fm,input_var)
    return data

def initialize_input(input_datafile):
    """
    Initializes the input from the alloy file and returns a list containing all contents.

    Parameters:
        input_datafile (str): Path to the input data file.

    Returns:
        tuple: A tuple containing three lists:
            fm (list): List of 62 variables from the file.
            fm_desc (list): List of descriptions for the variables.
            fm_explained (list): List of explanations for the variables.
    """
    
    i_df = open(input_datafile, "r")
    input_filedata = i_df.read() # read datafile into a list of strings, each string is a row containing one data variable
    input_filelines = input_filedata.splitlines()
    i_df.close()
    
    list_elements = 62  # len(file_lines) , but use fixed values for now
    fm = [0]*list_elements  # list of variables
    fm_desc = [0]*list_elements
    fm_explained = [0]*list_elements
    
    for i in range(list_elements):
        # the files use these spots for the variables
        float_string = input_filelines[i][0:16].strip()
        fm[i] = float(float_string)
        fm_desc_string = input_filelines[i][20:34].strip()
        fm_desc[i] = fm_desc_string
        fm_explained_string = input_filelines[i][34:].strip()
        fm_explained[i] = fm_explained_string

        
    return fm, fm_desc, fm_explained

def initialize_table():
    """
    Sets standard values for specific strain conditions.

    Returns:
        tuple: A tuple containing:
            input_values (list): Standard values for strain conditions.
            input_desc (list): Descriptions of the input values.
    """
    
    t0_in = 293                # Temperature [K]
    gamdot0_in = 0.01          # strain rate dEps/dt [1/s] 
    strain_end = 5             # initial concentration
    d0_um = 50                 # grain size in micrometer, corrected later
    m_taylor_in = 3            # Taylor factor
    input_values = [strain_end, t0_in , gamdot0_in , m_taylor_in,  d0_um]
    input_desc = ['Strain' , 'Temperature [K]' , 'Strainrate [1/s]' , 'Taylor factor', 'Grain size [\u00B5 m]']
    return input_values, input_desc

def expand_list(old_list, new_length):
    """
    Expands a list and makes linear regressions at value flips.

    Parameters:
        old_list (list): The original list of values.
        new_length (int): The desired length of the new list.

    Returns:
        list: The expanded list.
    """
    
    new_length = new_length-1
    new_list = []
    #checking if all values are the same
    if old_list.count(old_list[0])==len(old_list):
        new_list = [old_list[0]]*(new_length+1)
        
    else:
        full_list = np.array([])
        for i in range(len(old_list)-1):
            segment = np.linspace(old_list[i], old_list[i+1], new_length+1)
            segment = segment[0:-1:]
            full_list = np.concatenate((full_list, segment))
        new_list = full_list[0::len(old_list)-1]
        new_list = np.append(new_list,old_list[-1])
    
    return list(new_list)

def use_datafile(filename, input_datafile):
    """
    Replaces the alloy file with the specified file.

    Parameters:
        filename (str): The name of the file to use.
        input_datafile (str): The path to the input data file.
    """
    
    input_filepath = "./data/" + str(filename)
    if input_filepath: 
        shutil.copyfile(input_filepath, input_datafile)



