'''
Author: Daniel Preminger
Date: 20.12.2023

As part of a specialization project at the Department of Materials Science at NTNU.
'''

import numpy as np
from scipy.optimize import basinhopping  
from alflow_functions import running_alflow
import copy



def run_basinhopping(initial_parameters, experimental_data_list, fm, parameter_indices_to_optimize,
                     experimental_input_var, parameter_bounds, n_iterations):
    """
    Optimize a set of parameters using the basinhopping algorithm.

    Parameters:
        initial_parameters (list): Initial values for the parameters to be optimized.
        experimental_data_list (list of lists): List of experimental data sets for comparison.
        fm (list): The full list of parameters for the model.
        parameter_indices_to_optimize (list): Indices of the parameters in `fm` to be optimized.
        experimental_input_var (list of lists): List of input variable sets for each experimental data set.
        parameter_bounds (list of tuples): Bounds for the parameters to be optimized.
        n_iterations (int): Number of iterations for the basinhopping algorithm.

    Returns:
        list: The optimized parameters.
    """
    
    fm_copy = copy.deepcopy(fm)
    def wrapped_objective_function(parameters):
        """
        Objective function to minimize. Updates the model parameters and calculates
        the mean squared error between model outputs and experimental data.

        Parameters:
            parameters (list): Current values of the parameters being optimized.

        Returns:
            float: Total mean squared error for all experimental data sets.
        """
        
        for i in range(len(parameters)):
            fm_copy[parameter_indices_to_optimize[i]] = parameters[i]

        total_error = 0.0
        for i in range(len(experimental_data_list)):
            experimental_data_i = experimental_data_list[i]
            model_output_i = running_alflow(fm_copy, experimental_input_var[i])[2]
            error_i = np.mean((model_output_i - experimental_data_i) ** 2)
            total_error += error_i
        print(total_error)  
        return total_error

    bounds = tuple(parameter_bounds)
    minimizer_kwargs = {
        "method": "Nelder-Mead",
        "bounds": bounds,
    }
    
    
    result = basinhopping(wrapped_objective_function, initial_parameters, 
                          minimizer_kwargs=minimizer_kwargs, niter=n_iterations, stepsize=1, disp=True)
    optimized_parameters = result.x
    return optimized_parameters

