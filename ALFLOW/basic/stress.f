c----------------------------------------------------------
      Subroutine smooth_rhoi(rhoi_eff, rhoi, delta)
c----------------------------------------------------------

c  -- efficient rhoi_eff calculated from given substructure
c  -- where a statistical subgrainsize distribution 
c  -- is taken into account

      implicit none

c  -- transfered
      double precision  rhoi_eff, rhoi, delta

c  -- include common blocks and their declarations
      include "cmnprm.f"

c  -- local variables
      integer i
      real*8 x, f_III, delta3, kfakultet
      real*8 gam4, gam5, gam7, dlt_sc_inv

      x = Sqrt(rhoi)*delta/q_c
      delta3 = (0.5-0.5*tanh(2*(x-1.)))*q_c/sqrt(rhoi)
      x = 5.*delta3/delta
      gam4 = 1.
      kfakultet = 1.
      do i = 1,4
         kfakultet = kfakultet*i
         gam4=gam4 + x**i/kfakultet
      enddo
      kfakultet = kfakultet*5
      gam5 = gam4 + x**5/kfakultet
      kfakultet = kfakultet*6
      gam7 = gam5 + x**6/kfakultet + x**7/(kfakultet*7) 
      f_III = 1.-gam7*exp(-x)
      dlt_sc_inv = gam4/(gam5*delta)

      rhoi_eff = f_III*rhoi + (1.-f_III)*(q_c*dlt_sc_inv)**2
c      rhoi_eff=rhoi
      
      return
      end

c----------------------------------------------------------
      Subroutine stress_a(tau_a, rhoi, delta, phi, D, G, T)
c----------------------------------------------------------

c  -- athermal stress contributions from given substructure
c  -- a statistical subgrainsize distribution 
c  -- is taken into account

      implicit none

c  -- transfered
      double precision  tau_a, rhoi, delta, phi, D, G, T

c  -- include common blocks and their declarations
      include "cmnprm.f"
      
      tau_a = alpha1*G*b*Sqrt(rhoi)
     &     + alpha2*G*b/delta 
     &     + alpha2*G*b/D

c     clustering contribution at low temperatures
      tau_a = tau_a + tau_cl*(1.-tanh((T-T_cl)/(T_cl*dT_cl)))*0.5

      return
      end

c------------------------------------------------
      Subroutine stress_p(tau_p, G)
c------------------------------------------------

c     Particle stress from precipitates, constitutive and dispersoids

      implicit none
c  -- transfered
      double precision  tau_p, G

C  -- f2py signature:
Cf2py intent(out) :: tau_p
Cf2py intent(in)  :: G

c  -- include common blocks and their declarations
      include "cmnprm.f"

c  -- local
      double precision pi
      parameter(pi=3.141592654)
      double precision lambda, tau_disp, tau_prim

c  -- dispersoids
      if (f_disp.lt.1.0E-10) then
         tau_disp = 0.
      else
         lambda = 0.8*(sqrt(pi/f_disp)-2.)*r_disp
         tau_disp = A*G*b*log(lambda/b)/(7.791149*lambda)
      endif

c  -- consitutive particles
      if (f_prim.lt.1.0E-10) then
         tau_prim = 0.
      else
         lambda = 0.8*(sqrt(pi/f_prim)-2.)*r_prim
         tau_prim = A*G*b*log(lambda/b)/(7.791149*lambda)
      endif

c  -- total stress
c      tau_p = tau_prec + tau_disp + tau_prim
      tau_p = tau_prec + sqrt(tau_disp**2 + tau_prim**2)

      return
      end



c------------------------------------------------
      Subroutine stress_t(tau_t,rho,gamdot,G,T,rho_m)
c------------------------------------------------

c     Calculate the thermal component of the stress, 
c     due to short range interactions between mobile dislocation
c     loops and the stored network.

c --  Decide which kind of recovery are allowed?
c --  binary system: RC_mode = xyz, where xyz are digits
c --  equal to 0 if false or 1 if true. 
c --  x = pure metal climb (pm_c)
c --  y = solute glide     (s_g )
c --  z = solute climb     (s_c )
c
c     RC_mode=7: pm_c + s_g + s_c
c     RC_mode=6: pm_c + s_g
c     RC_mode=5: pm_c       + s_c
c     RC_mode=4: pm_c
c     RC_mode=3:        s_g + s_c
c     RC_mode=2:        s_g
c     RC_mode=1:              s_c
 
c     In the recovery terms the formula 
c        arg_pre *exp(arg_exp) *2. *sinh(argsinh)
c     is exception handled in order to avoid numerical overflow:
c   * In order to better handle extreme values we use:
c        2*exp(x)*sinh(y) = exp(x+y)-exp(x-y)
c   * Exception:  y<<1 and x>>1, => inf - inf. Asymptotic:
c        exp(x)*sinh(y)  ->  exp(ln(y)+x), for y<<1.
c   * During time-step iterations the exp() terms still
c       can become to large in the not yet converged solution.
c       Remedy: cutt-off:  exp() < exp(100.)

      implicit none

c  -- transfered
      double precision tau_t, rho, gamdot, G, T, rho_m

C  -- f2py signature:
Cf2py intent(out) :: tau_t
Cf2py intent(in)  :: rho
Cf2py intent(in)  :: gamdot
Cf2py intent(in)  :: G
Cf2py intent(in)  :: T
Cf2py intent(in)  :: rho_m

c  -- include common blocks and their declarations
      include "cmnprm.f"

c  -- local variables
      double precision pre_exp, arg_exp, arg_sinh
      double precision v_s, v_pm, pre_asinh, arg_pre
      double precision exp_term
      double precision tau_t_pm, tau_t_s
      double precision conc_sc

c  -- functions
      double precision asinhexp, exp2sinh

      v_s      = 0.0
      v_pm     = 0.0
      tau_t_pm = 0.0
      tau_t_s  = 0.0
      

c     avoid numerical problem when gamdot=0
      if(gamdot.eq.0.) then
         tau_t = 0.
         return
      endif


c  -- first calculate the possible stresses and 
c  -- velocities in the Orowan expression 
c  -- for the two cases pm and s:

c     case= pm , "pure metal"
      If(RC_mode .ge. 4) then
         arg_pre   = b* B_t_pm* nu_Debye
         arg_exp   = U_t_pm/ (k* T)
         pre_asinh = k*T*Sqrt(rho)/(omega_rho*b**2)
         pre_exp   = 0.5*gamdot/(rho_m*b*arg_pre)
         exp_term  = arg_exp + log(pre_exp)
         tau_t_pm  = pre_asinh*asinhexp(exp_term)
c     -- velocity terms
         arg_sinh  = tau_t_pm/pre_asinh
         v_pm      = arg_pre* exp2sinh(-arg_exp,arg_sinh)
      endif
c     case= s , "solute"

      if(RC_mode.ne.4) then
         call conc_AF(conc_sc, rho_m, T, gamdot)
         arg_pre   = b* conc_sc**(e_t-1.)* B_t_s* nu_Debye/omega_t
         arg_exp   = U_t_s/(k*T)
         pre_asinh = k*T/(conc_sc**(-e_t)* omega_t* b**3)
         pre_exp   = 0.5*gamdot/(rho_m*b*arg_pre)
         exp_term  = arg_exp + log(pre_exp)
         tau_t_s   = pre_asinh*asinhexp(exp_term)
c     -- velocity terms
         arg_sinh  = tau_t_s/pre_asinh
         v_s       = arg_pre* exp2sinh(-arg_exp,arg_sinh)
      endif

c  -- finally chose the minimum velocity (i.e. min. frequency)
c  -- or according to RC_mode (see header comments)

      If (RC_mode .ge. 5 .and. RC_mode .le. 7) Then
         If (v_s .lt. v_pm) then
            tau_t = tau_t_s
         Else
            tau_t = tau_t_pm
         EndIf
      ElseIf (RC_mode .eq. 4) Then
         tau_t = tau_t_pm
      ElseIf (RC_mode .le. 3 .and. RC_mode .ge. 1) Then
         tau_t = tau_t_s
      Else
         Write(*,*) "RC_mode = ", RC_mode," is not allowed"
         Write(*,*) "Please put RC_mode in the range 1-7"
      EndIf
      return
      end
