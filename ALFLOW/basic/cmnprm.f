c --  common block variables

      Double precision alpha1, alpha2, k, b, nu_Debye, B_t_pm, B_t_s, 
     &     omega_rho, q_c, q_b, f, conc, A, nu, kappa_0, C, m,
     &     omega_t, U_t_s, U_t_pm, e_t, kappa_2, phi_IV,q_IV,
     &     B_rho_pm, B_rho_sg, B_rho_sc, ksi_rho_pm, ksi_rho_sg,
     &     ksi_rho_sc, omega_s, e_rho, U_SD, U_s, dU_s, S_sc, S_II, S_IV 
     &     , B_delta_pm, B_delta_s, ksi_delta_pm, ksi_delta_s, e_delta,
     &     phi_c, conc_0, n_sc, dU_sc, B_sc, tau_cl, T_cl, dT_cl,
     &     tau_prec,
     &     kappa3_prec, f_prec, r_prec, 
     &     kappa3_disp, f_disp, r_disp,
     &     kappa3_prim, f_prim, r_prim

c     &     omega_rho, q_c, q_b, f, conc, A, f_r, r, nu, kappa_0, C, m,
c     &     omega_t, U_t_s, U_t_pm, e_t, kappa_2, kappa_3, phi_IV,q_IV,
c     &     delta0, delta_III, kappa_4

      Integer RC_mode


C  -- common blocks

      common/AFparam/ alpha1, alpha2, k, b, nu_Debye, B_t_pm, B_t_s, 
     &     omega_rho, q_c, q_b, f, conc, A, nu, kappa_0, C, m,
     &     omega_t, U_t_s, U_t_pm, e_t, kappa_2, phi_IV,q_IV,
     &     B_rho_pm, B_rho_sg, B_rho_sc, ksi_rho_pm, ksi_rho_sg,
     &     ksi_rho_sc, omega_s, e_rho, U_SD, U_s, dU_s, S_sc, S_II, S_IV 
     &     , B_delta_pm, B_delta_s, ksi_delta_pm, ksi_delta_s, e_delta,
     &     phi_c, conc_0, n_sc, dU_sc, B_sc,tau_cl,T_cl,dT_cl,
     &     tau_prec,
     &     kappa3_prec, f_prec, r_prec, 
     &     kappa3_disp, f_disp, r_disp,
     &     kappa3_prim, f_prim, r_prim,
     &     RC_mode

c     &     omega_rho, q_c, q_b, f, conc, A, f_r, r, nu, kappa_0, C, m,
c     &     omega_t, U_t_s, U_t_pm, e_t, kappa_2, kappa_3, phi_IV,q_IV,
c     &     delta0, delta_III, kappa_4,
