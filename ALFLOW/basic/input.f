      subroutine input_AF(uin, rho_i, delta, phi, D00)
c
c     Read input parameters from unit number UIN and provide initial
c     values for the state variables: rhoi, delta, phi and D.
c 
c ON INPUT:
c     uin     integer            Unit number to read input from.

c ON OUTPUT:
c     rhoi    double precision   Initial interior dislocation density, #/m^2.
c     delta   double precision   Initial subgrain size, m.
c     phi     double precision   Initial misorientation, rad.
c     D00     double precision   Initial grain size, m.
c
      implicit none

c  -- transfered in the call
      integer uin
      double precision  rho_i,delta,phi,D00

c  -- local declarations
      double precision Na, tmp1, facS_IV

c  -- include common blocks and their declarations
      include "cmnprm.f"


c  What kind of recovery to be allowed?
c  calculate RC_mode (between 1 and 7) using the following formula:
c      RC_mode = 4*pm+2*sg+sc,  where  
c  pure metal recovery  :  pm=1 if allowed, else 0.
c  solute glide recovery:  sg=1 if allowed, else 0.
c  solute climb recovery:  sc=1 if allowed, else 0.
      read(uin,*) RC_mode 
C  Material and alloy specific data
      read(uin,*) nu_Debye
      read(uin,*) b

c    Boltzmann
      read(uin,*) k
c     Poisson ratio (Elastic)
      read(uin,*) nu
C  ALLOYING
      read(uin,*) conc
      read(uin,*) conc_0
      read(uin,*) n_sc
      read(uin,*) dU_sc
      read(uin,*) B_sc
c  SCALING parameters
      read(uin,*) q_c
      read(uin,*) C
c     Volume fraction of cell-walls in stage II
      read(uin,*) f
c  Parameters relating microstructure and flow stress
      read(uin,*) alpha1
      read(uin,*) alpha2
c  relating subgrain-size to total dislocation density
      read(uin,*) kappa_0
C  Activation energies , remember particle or mass
      read(uin,*) U_t_pm
      read(uin,*) U_t_s
      read(uin,*) U_SD
      read(uin,*) U_s
      read(uin,*) dU_S
C     per particle
      Na      = 6.022e+23
      U_t_pm  = U_t_pm/Na
      U_t_s   = U_t_s/Na
      U_SD    = U_SD/Na
      U_s     = U_s/Na
      dU_s    = dU_s/Na
      dU_sc   = dU_sc/Na

      read(uin,*) kappa_2

      read(uin,*) A     
      read(uin,*) phi_c
C
C  Thermal tau constants
      read(uin,*) B_t_pm
      read(uin,*) B_t_s
      read(uin,*) omega_rho
      read(uin,*) omega_t
      read(uin,*) e_t
c     rho_m = m * rho
      read(uin,*) m

c     rho_i recovery constants
      read(uin,*) B_rho_pm
      read(uin,*) B_rho_sg
      read(uin,*) B_rho_sc

      read(uin,*) ksi_rho_pm
      read(uin,*) ksi_rho_sg
      read(uin,*) ksi_rho_sc

      read(uin,*) omega_s
      read(uin,*) e_rho

c     delta recovery constants
      read(uin,*) B_delta_pm
      read(uin,*) B_delta_s
      read(uin,*) ksi_delta_pm
      read(uin,*) ksi_delta_s
c     new parameter in v1.02
      read(uin,*) e_delta

c     read S_IV/S_sc
      read(uin,*) facS_IV 


c     parameters, misorientation
      read(uin,*) q_IV
      read(uin,*) phi_IV

C-----------------------------------
C---  the initial conditions --------

      read(uin,*) rho_i
      read(uin,*) D00

c     New parameters added in order to take into account 
c     solute of low diffusivity at RT
      
      Read (uin,*) tau_cl 
      Read (uin,*) T_cl 
      Read (uin,*) dT_cl 
      Read (uin,*) q_b

c-----------------------------------
c     strength contribution from precipitates
      read (uin,*) tau_prec

c-----------------------------------
c     slip length factors, volume fractions and radii for non-sherable 
c     particle classes
      read (uin,*) kappa3_prec
      read (uin,*) f_prec
      read (uin,*) r_prec 
      read (uin,*) kappa3_disp 
      read (uin,*) f_disp
      read (uin,*) r_disp
      read (uin,*) kappa3_prim
      read (uin,*) f_prim
      read (uin,*) r_prim

C---  Parameters that in the future should be added to the input file ---

c     New parameters for the grain size effect. For now, we just
c     set them to default values that will turn off the grain size 
c     effect...
c      delta0 = 1
c      delta_III = 1e-6
c      kappa_4 = 0


c     q_b from q_c ,alpha1, alpha2 and f if q_b < 1.
      if(q_b.lt.1) then
         tmp1   = alpha2/(alpha1*q_c)
         q_b = sqrt(tmp1*(tmp1+2.)/f +1.)
         write(*,*) "input q_b less than 1, set to: ", q_b 
      endif

c     Scaling constants S in delta equation (decrease term)
      S_sc   = f*q_b**2./(2.*(C**2.)*(1-f))

c     Stage II scaling parameter
      S_II   = f*(q_b**2-1.)/(2.* C**2)

C     calculates S_IV from S_sc
      S_IV   = facS_IV*S_sc

c     delta and phi default initial conditions
      delta = q_c/Sqrt(rho_i)
      phi   = b*q_c*f*(q_b**2-1.)*sqrt(rho_i)/kappa_0

      Return
      End





